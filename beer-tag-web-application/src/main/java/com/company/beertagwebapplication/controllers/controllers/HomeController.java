package com.company.beertagwebapplication.controllers.controllers;

import com.company.beertagwebapplication.controllers.ControllerHelper;
import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.services.interfaces.BeerService;
import com.company.beertagwebapplication.services.interfaces.CountryService;
import com.company.beertagwebapplication.services.interfaces.StyleService;
import com.company.beertagwebapplication.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController extends ControllerHelper {
    private BeerService beerService;
    private CountryService countryService;
    private StyleService styleService;
    private TagService tagService;

    @Autowired
    public HomeController(BeerService beerService, CountryService countryService, StyleService styleService, TagService tagService) {
        this.beerService = beerService;
        this.countryService = countryService;
        this.styleService = styleService;
        this.tagService = tagService;
    }

    @GetMapping("/")
    public String showHomePage(Model model, @RequestParam(required = false) String tags,
                               @RequestParam(required = false) String style,
                               @RequestParam(required = false) String country,  @PageableDefault(size = 12) Pageable pageable) {

        Page<Beer> beers = beerService.get(extractTags(tags), style, country, pageable);

        model.addAttribute("beers", beers);

        model.addAttribute("countries", getCountriesList(countryService));

        model.addAttribute("styles", getStylesList(styleService));

        model.addAttribute("tags", getTagsList(tagService));

        return "index";
    }

    @GetMapping("/admin")
    public String showAdminPage(){
        return "admin";
    }

    @GetMapping("/about")
    public String showAboutPage(){
        return "about";
    }
}
