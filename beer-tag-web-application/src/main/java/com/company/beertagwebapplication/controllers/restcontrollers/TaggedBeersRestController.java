package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.TaggedBeer;
import com.company.beertagwebapplication.services.interfaces.TaggedBeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/beers")
public class TaggedBeersRestController {
    private TaggedBeersService taggedBeersService;

    @Autowired
    public TaggedBeersRestController(TaggedBeersService taggedBeersService) {
        this.taggedBeersService = taggedBeersService;
    }

    @PostMapping("/tags/add")
    public TaggedBeer add(@RequestBody TaggedBeer taggedBeer) {
        try {
            taggedBeersService.add(taggedBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return taggedBeer;
    }

    @DeleteMapping("/tags/remove")
    public int remove(@RequestBody TaggedBeer taggedBeer) {
        try {
            return taggedBeersService.remove(taggedBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
