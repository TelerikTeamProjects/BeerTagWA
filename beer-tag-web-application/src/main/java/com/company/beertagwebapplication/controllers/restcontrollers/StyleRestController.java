package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Style;
import com.company.beertagwebapplication.services.interfaces.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {
    private StyleService styleService;

    @Autowired
    public StyleRestController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping
    public List<Style> getAll(@Valid @RequestParam(required = false) String name) {
        return styleService.get(name);
    }

    @GetMapping("/{id}")
    public Style getById(@Valid @PathVariable int id) {
        try {
            return styleService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public Style create(@Valid @RequestBody Style style) {
        try {
            styleService.create(style);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return style;
    }

    @PutMapping("/{id}")
    public int update(@Valid @PathVariable int id, @RequestBody Style style) {
        try {
            return styleService.update(id, style);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
