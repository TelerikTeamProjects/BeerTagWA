package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Tag;
import com.company.beertagwebapplication.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {
    private TagService tagService;

    @Autowired
    public TagRestController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public List<Tag> getAll(@Valid @RequestParam(required = false) String name) {
        return tagService.get(name);
    }

    @GetMapping("/{id}")
    public Tag getById(@Valid @PathVariable int id) {
        try {
            return tagService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public Tag create(@RequestBody Tag tag) {
        try {
            tagService.create(tag);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return tag;
    }

    @PutMapping("/{id}")
    public int update(@Valid @PathVariable int id, @RequestBody Tag tag) {
        try {
            return tagService.update(id, tag);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
