package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.RatedBeer;
import com.company.beertagwebapplication.services.interfaces.RatedBeerService;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class RatedBeerRestController {
    private RatedBeerService ratedBeerService;

    @Autowired
    public RatedBeerRestController(RatedBeerService ratedBeerService) {
        this.ratedBeerService = ratedBeerService;
    }

    @PostMapping("/rate")
    public RatedBeer add(@RequestBody RatedBeer ratedBeer) {

        try {
            ratedBeerService.add(ratedBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return ratedBeer;
    }

    @GetMapping("/rating")
    @ResponseBody
    public int getRating(@RequestParam int beerId, @RequestParam int userId) {
        RatedBeer ratedBeer = new RatedBeer();
        Beer beer = new Beer();
        beer.setId(beerId);

        ratedBeer.setUserId(userId);
        ratedBeer.setBeer(beer);
        List<RatedBeer> resultList = ratedBeerService.get(ratedBeer);

        if (resultList.size() > 0) {
            return resultList.get(0).getBeerRating();
        }
        return 0;
    }
}
