package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.MarkedBeer;
import com.company.beertagwebapplication.services.interfaces.MarkedBeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
//TODO Remove user from endpoint
@RequestMapping("/api/user/marks")
public class MarkedBeerRestController {
    private MarkedBeerService markedBeerService;

    @Autowired
    public MarkedBeerRestController(MarkedBeerService markedBeerService) {
        this.markedBeerService = markedBeerService;
    }

    @GetMapping("/get")
    public List<MarkedBeer> get(@RequestParam int beerId, @RequestParam int userId, @RequestParam int markId) {
        MarkedBeer markedBeer = new MarkedBeer();
        Beer beer = new Beer();
        beer.setId(beerId);
        markedBeer.setBeer(beer);
        markedBeer.setMarkId(markId);
        markedBeer.setUserId(userId);
        return markedBeerService.get(markedBeer);
    }

    @PostMapping("/new")
    public int add(@RequestBody MarkedBeer markedBeer) {
        try {
            markedBeerService.add(markedBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        return markedBeer.getId();
    }

    @PutMapping("/update")
    public int update(@RequestBody MarkedBeer markedBeer) {
        try {
            return markedBeerService.update(markedBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/remove")
    public int remove(@RequestBody MarkedBeer markedBeer) {
        try {
            return markedBeerService.remove(markedBeer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
