package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.MarkedBeer;
import com.company.beertagwebapplication.models.RatedBeer;
import com.company.beertagwebapplication.models.UserDetails;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserDetailsManager userDetailsManager;
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserRestController(UserDetailsManager userDetailsManager, UserService userService, PasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public List<UserDetails> getAll(@RequestParam (required = false) String username) {
        return userService.get(username);
    }

    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable int id) {
        try {
            return userService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}/topbeers")
    public List<RatedBeer> getUserTopBeers(@PathVariable int id){
        try {
            return userService.getRatedBeers(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{id}/wishlist")
    public List<MarkedBeer> getUserWishlist(@PathVariable int id){
        try {
            return userService.getMarkedBeers(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/sign-up")
    public UserDetails create(@Valid @RequestBody UserDetails userDetails) {
        userService.create(userDetails);
        return userDetails;
    }

    @PutMapping("/{id}")
    public int update(@PathVariable int id, @RequestBody UserDetails userDetails) {
        try {
            return userService.update(id, userDetails);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/{role}")
    public void changeRole(@PathVariable int id, @PathVariable String role) {
        try {
            UserDetails userDetails = userService.getById(id);
            String username = userDetails.getUsername();
            org.springframework.security.core.userdetails.UserDetails oldDetails = userDetailsManager.loadUserByUsername(username);

            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList();
            
            if(role.equals("admin")){
                authorities = AuthorityUtils.createAuthorityList("ROLE_ADMIN");
            }

            org.springframework.security.core.userdetails.UserDetails newDetails = new User(username, oldDetails.getPassword(),  authorities);
            userDetailsManager.updateUser(newDetails);

        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public int delete(@PathVariable int id) {
        try {
            UserDetails userDetails = userService.getById(id);
            String username = userDetails.getUsername();


            List<GrantedAuthority> authorities = AuthorityUtils.NO_AUTHORITIES;

            org.springframework.security.core.userdetails.UserDetails oldDetails = userDetailsManager.loadUserByUsername(username);
            org.springframework.security.core.userdetails.UserDetails newDetails = new User(username, oldDetails.getPassword(), false, true, true, true,   authorities);
            userDetailsManager.updateUser(newDetails);

           return userService.delete(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
