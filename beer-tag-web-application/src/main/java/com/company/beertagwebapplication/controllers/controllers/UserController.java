package com.company.beertagwebapplication.controllers.controllers;

import com.company.beertagwebapplication.models.UserDTO;
import com.company.beertagwebapplication.models.UserDetails;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {
    private UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @GetMapping("/admin/users")
    public String showUsers(Model model, @RequestParam(required = false) String name) {
        List<UserDetails> users = service.get(name);
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/admin/new")
    public String createUsers(Model model, @RequestParam(required = false) String name) {
        model.addAttribute("user", new UserDTO());
        return "user";
    }

    @GetMapping("/users/new")
    public String showNewUserView(Model model) {
        model.addAttribute("user", new UserDetails());
        return "user";
    }

    @PostMapping("/users/new")
    public String createUser(@Valid @ModelAttribute UserDetails user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "user";
        }
        service.create(user);
        return "redirect:/users";
    }
}

