package com.company.beertagwebapplication.controllers.controllers;

import com.company.beertagwebapplication.models.UserDTO;
import com.company.beertagwebapplication.models.UserDetails;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService, PasswordEncoder passwordEncode) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
        this.passwordEncoder = passwordEncode;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDTO());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDTO user, BindingResult bindingResult, HttpServletRequest request, Model model) throws ServletException{
        if (bindingResult.hasErrors()) {
            if (bindingResult.getFieldError("username") != null) {
                model.addAttribute("error_username", bindingResult.getFieldError("username").getDefaultMessage());
            }
            if (bindingResult.getFieldError("password") != null) {
                model.addAttribute("error_password", bindingResult.getFieldError("password").getDefaultMessage());
            }
            if (bindingResult.getFieldError("firstName") != null) {
                model.addAttribute("error_firstName", bindingResult.getFieldError("firstName").getDefaultMessage());
            }
            if (bindingResult.getFieldError("lastName") != null) {
                model.addAttribute("error_lastName", bindingResult.getFieldError("lastName").getDefaultMessage());
            }
            user.setPassword(null);
            model.addAttribute("user", user);
            return "register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(), passwordEncoder.encode(user.getPassword()), authorities);

        try {
            userDetailsManager.createUser(newUser);
        } catch (Exception e) {
            model.addAttribute("error_username_exists", "This username already exists and drinks beer!");
            user.setPassword(null);
            model.addAttribute("user", user);
            return "register";
        }

        UserDetails userDetails = new UserDetails();
        userDetails.setUsername(user.getUsername());
        userDetails.setFirstName(user.getFirstName());
        userDetails.setLastName(user.getLastName());
        userDetails.setActive(true);
        userService.create(userDetails);

        request.login(user.getUsername(), user.getPassword());
        return "redirect:/";
        //TODO split redirection
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        if(principal == null) {
//            request.login(user.getUsername(), user.getPassword());
//            return "redirect:/";
//        }else{
//            return "redirect:admin/users";
//        }
    }
}
