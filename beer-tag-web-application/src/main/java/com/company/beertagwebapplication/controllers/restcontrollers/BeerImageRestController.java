package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.BeerImage;
import com.company.beertagwebapplication.payload.UploadFileResponse;
import com.company.beertagwebapplication.services.BeerImageServiceImpl;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;

@RestController
@RequestMapping("/api/beerimages")
public class BeerImageRestController {
    private BeerImageServiceImpl beerImageService;
    private UserService userService;

    @Autowired
    public BeerImageRestController(BeerImageServiceImpl beerImageService, UserService userService) {
        this.beerImageService = beerImageService;
        this.userService = userService;
    }

    @PostMapping("/upload/{beerId}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int beerId) {
        if(file == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Filename is invalid.", file.getOriginalFilename()));
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getName().equals("anonymousUser")) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid username."));
        }
        int userId = userService.get(authentication.getName()).get(0).getId();

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        BeerImage image;

        if (!fileName.matches("([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)")) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Filename is invalid", fileName));
        }

        try {
            image = new BeerImage(1, fileName, file.getContentType(), file.getBytes());
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Image could not be stored. Please try again later!"));
        }

        try {
           beerImageService.storeFile(image, beerId, userId);
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/beerimages/download/")
                .path(String.format("%d",image.getId()))
                .toUriString();

        return new UploadFileResponse(image.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @GetMapping("/{fileId}")
    public ResponseEntity<Resource> get(@PathVariable int fileId) {
        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(beerImageService.getFile(fileId).getFileType()))
                    .body(new ByteArrayResource(beerImageService.getFile(fileId).getImage()));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/download/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable int fileId) {
        BeerImage dbFile;
        try {
            dbFile = beerImageService.getFile(fileId);
        }catch(IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }


        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getImage()));
    }
}