package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.UserImage;
import com.company.beertagwebapplication.payload.UploadFileResponse;
import com.company.beertagwebapplication.services.UserImageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;

@RestController
@RequestMapping("/api/userimages")
public class UserImageRestController {
    private UserImageServiceImpl userImageService;

    @Autowired
    public UserImageRestController(UserImageServiceImpl userImageService) {
        this.userImageService = userImageService;
    }

    @PostMapping("/upload/{userId}")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable int userId) {
        if(file == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Filename is invalid", file.getOriginalFilename()));
        }

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        UserImage image;

        if (!fileName.matches("([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)")) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Filename is invalid", fileName));
        }

        try {
            image = new UserImage(1, fileName, file.getContentType(), file.getBytes());
        } catch (IOException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("Image could not be stored. Please try again later!"));
        }

        try {
            userImageService.storeFile(image, userId);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/userimages/download/")
                .path(String.format("%d", image.getId()))
                .toUriString();

        return new UploadFileResponse(image.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @GetMapping("/{fileId}")
    public ResponseEntity<Resource> get(@PathVariable int fileId) {
        try {
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(userImageService.getFile(fileId).getFileType()))
                    .body(new ByteArrayResource(userImageService.getFile(fileId).getImage()));
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/download/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable int fileId) {
        UserImage dbFile = userImageService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dbFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getFileName() + "\"")
                .body(new ByteArrayResource(dbFile.getImage()));
    }
}