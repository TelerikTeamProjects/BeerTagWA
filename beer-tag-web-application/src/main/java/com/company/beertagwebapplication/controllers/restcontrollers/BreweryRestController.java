package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Brewery;
import com.company.beertagwebapplication.services.interfaces.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {
    private BreweryService breweryService;

    @Autowired
    public BreweryRestController(BreweryService breweryService) {
        this.breweryService = breweryService;
    }

    @GetMapping
    public List<Brewery> getAll(@Valid @RequestParam(required = false) String name) {
        return breweryService.get(name);
    }

    @GetMapping("/{id}")
    public Brewery getById(@Valid @PathVariable int id) {
        return breweryService.getById(id);
    }

    @PostMapping("/new")
    public Brewery create(@Valid @RequestBody Brewery brewery) {
        try {
            breweryService.create(brewery);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return brewery;
    }

    @PutMapping("/{id}")
    public int update(@Valid @PathVariable int id, @RequestBody Brewery brewery) {
        try{
            return breweryService.update(id, brewery);
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
