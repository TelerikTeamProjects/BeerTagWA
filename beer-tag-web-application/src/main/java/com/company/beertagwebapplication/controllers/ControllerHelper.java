package com.company.beertagwebapplication.controllers;

import com.company.beertagwebapplication.models.Brewery;
import com.company.beertagwebapplication.models.Country;
import com.company.beertagwebapplication.models.Style;
import com.company.beertagwebapplication.models.Tag;
import com.company.beertagwebapplication.services.interfaces.BreweryService;
import com.company.beertagwebapplication.services.interfaces.CountryService;
import com.company.beertagwebapplication.services.interfaces.StyleService;
import com.company.beertagwebapplication.services.interfaces.TagService;

import java.util.ArrayList;
import java.util.List;

public class ControllerHelper {
    public List<Tag> extractTags(String tagsString) {
        if (tagsString == null) {
            return null;
        }
        List<Tag> result = new ArrayList<>();
        String[] strings = tagsString.split("#");

        for (String str : strings) {
            String[] temp = str.split(",");
            if (temp.length == 2) {
                result.add(new Tag(Integer.parseInt(temp[0]), temp[1]));
            }
        }
        return result;
    }

    public List<Style> getStylesList(StyleService styleService) {
        return styleService.get(null);
    }

    public List<Country> getCountriesList(CountryService countryService) {
        return countryService.get(null);
    }

    public List<Tag> getTagsList(TagService tagService) {
        return tagService.get(null);
    }

    public List<Brewery> getBreweryList(BreweryService breweryService) {
        return breweryService.get(null);
    }
}
