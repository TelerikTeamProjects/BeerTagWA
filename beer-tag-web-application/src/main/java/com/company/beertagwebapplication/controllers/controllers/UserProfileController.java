package com.company.beertagwebapplication.controllers.controllers;

import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Base64;
import java.util.Collection;

@Controller
public class UserProfileController {
    private UserService userService;

    @Autowired
    public UserProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users/userprofile/{userId}")
    public String showUserPage(Model model, @PathVariable int userId) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean isAdmin = false;
        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().equals("ROLE_SUPER_ADMIN") || authority.getAuthority().equals("ROLE_ADMIN")) {
                isAdmin = true;
            }
        }

        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }

        if (username.equals(userService.getById(userId).getUsername()) || isAdmin) {
            com.company.beertagwebapplication.models.UserDetails user = userService.getById(userId);
            model.addAttribute("user", user);
            if (user.getImage() != null) {
                model.addAttribute("image", Base64.getEncoder().encodeToString(user.getImage().getImage()));
            }

            return "userprofile";
        } else {
            return "error-404";
        }
    }
}

