package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.controllers.ControllerHelper;
import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.Tag;
import com.company.beertagwebapplication.services.interfaces.BeerService;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController extends ControllerHelper {
    private BeerService beerService;
    private UserService userService;

    @Autowired
    public BeerRestController(BeerService beerService, UserService userService) {
        this.beerService = beerService;
        this.userService = userService;
    }

    @GetMapping
    public Page<Beer> getAll(@RequestParam(required = false) String tags,  @RequestParam(required = false) String style,
                          @RequestParam(required = false) String country,  @PageableDefault(size = 12) Pageable pageable) {
        return beerService.get(extractTags(tags), style, country, pageable);
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return beerService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/tags")
    public List<Tag> getTagsById(@PathVariable int id) {
        try {
            return beerService.getById(id).getTags();
        } catch (NullPointerException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/new")
    public Beer create(@RequestBody Beer beer) {
        try {
            beerService.create(beer);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return beer;
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody Beer beer) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getName().equals("anonymousUser")) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid username."));
        }
        int userId = userService.get(authentication.getName()).get(0).getId();

        try {
            beerService.update(id, beer, userId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return beer;
    }

    @DeleteMapping("/{id}")
    public int delete(@PathVariable("id") int beerId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getName().equals("anonymousUser")) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid username."));
        }
        int userId = userService.get(authentication.getName()).get(0).getId();

        try {
            return beerService.delete(beerId, userId);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
