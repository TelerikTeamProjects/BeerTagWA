package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Country;
import com.company.beertagwebapplication.services.interfaces.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {
    private CountryService countryService;

    @Autowired
    public CountryRestController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping
    public List<Country> getAll(@Valid @RequestParam(required = false) String name) {
        return countryService.get(name);
    }

    @GetMapping("/{id}")
    public Country getById(@Valid @PathVariable int id) {
        return countryService.getById(id);
    }

    @PostMapping("/new")
    public Country create(@Valid @RequestBody Country country) {
        try {
            countryService.create(country);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return country;
    }

    @PutMapping("/{id}")
    public int update(@Valid @PathVariable int id, @RequestBody Country country) {
        try{
            return countryService.update(id, country);
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
