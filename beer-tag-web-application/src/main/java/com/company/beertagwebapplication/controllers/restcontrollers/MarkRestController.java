package com.company.beertagwebapplication.controllers.restcontrollers;

import com.company.beertagwebapplication.models.Mark;
import com.company.beertagwebapplication.services.interfaces.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/marks")
public class MarkRestController {
    private MarkService markService;

    @Autowired
    public MarkRestController(MarkService markService) {
        this.markService = markService;
    }

    @GetMapping
    public List<Mark> getAll(String name) {
        return markService.get(name);
    }

    @GetMapping("/{id}")
    public Mark getById(@Valid @PathVariable int id) {
        return markService.getById(id);
    }

    @PostMapping("/new")
    public Mark create(@Valid @RequestBody Mark mark) {
        try {
            markService.create(mark);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
        return mark;
    }

    @PutMapping("/{id}")
    public int update(@Valid @PathVariable int id, @RequestBody Mark mark) {
        try{
            return markService.update(id, mark);
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
