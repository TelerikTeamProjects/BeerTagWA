package com.company.beertagwebapplication.controllers.controllers;

import com.company.beertagwebapplication.controllers.ControllerHelper;
import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.services.interfaces.*;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
public class BeerController extends ControllerHelper {
    private BeerService beerService;
    private CountryService countryService;
    private StyleService styleService;
    private TagService tagService;
    private BreweryService breweryService;
    private UserService userService;

    @Autowired
    public BeerController(BeerService beerService, CountryService countryService, StyleService styleService,
                          TagService tagService, BreweryService breweryService, UserService userService) {
        this.beerService = beerService;
        this.countryService = countryService;
        this.styleService = styleService;
        this.tagService = tagService;
        this.breweryService = breweryService;
        this.userService = userService;
    }

    @GetMapping("/beers/new")
    public String showCreateBeerPage(Model model) {
        Beer beer = new Beer();

        model.addAttribute("beer", beer);

        model.addAttribute("countries", getCountriesList(countryService));
        model.addAttribute("styles", getStylesList(styleService));
        model.addAttribute("breweries", getBreweryList(breweryService));

        return "beerCreate";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute Beer beer, BindingResult bindingResult, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getName().equals("anonymousUser")) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid username."));
        }
        UserDetails owner = userService.get(authentication.getName()).get(0);

        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            if (bindingResult.getFieldError("name") != null) {
                model.addAttribute("error_name", bindingResult.getFieldError("name").getDefaultMessage());
            }
            model.addAttribute("beer", beer);
            model.addAttribute("countries", getCountriesList(countryService));
            model.addAttribute("styles", getStylesList(styleService));
            model.addAttribute("breweries", getBreweryList(breweryService));
            return "beerCreate";
        }

        beer.setActive(true);
        beer.setOwner(owner);

        try {
            beerService.create(beer);
        } catch (IllegalArgumentException e) {
            model.addAttribute("error_name_exists", String.format("Beer with name %s already exists.", beer.getName()));
            model.addAttribute("beer", beer);
            model.addAttribute("countries", getCountriesList(countryService));
            model.addAttribute("styles", getStylesList(styleService));
            model.addAttribute("breweries", getBreweryList(breweryService));
            return "beerCreate";
        }
        Beer resultBeer = beerService.getByName(beer.getName());

        return "redirect:/beers/" + resultBeer.getId() + "/edit";
    }

    @GetMapping("/beers/{id}")
    public String showBeerPage(Model model, @PathVariable int id) {
        Beer beer = getVerifiedBeer(id);

        model.addAttribute("beer", beer);
        //model.addAttribute("picture", Base64.getEncoder().encodeToString(beer.getImage().getImage()));
        if (beer.getImage() != null) {
            model.addAttribute("picture", Base64.encodeBase64String(beer.getImage().getImage()));
        }
        model.addAttribute("tags", getTagsList(tagService));

        return "beer";
    }

    @GetMapping("/beers/{id}/edit")
    public String showEditBeerPage(Model model, @PathVariable int id) {
        Beer beer = getVerifiedBeer(id);

        model.addAttribute("beer", beer);
        if (beer.getImage() != null) {
            model.addAttribute("picture", Base64.encodeBase64String(beer.getImage().getImage()));
        }
        model.addAttribute("countries", getCountriesList(countryService));
        model.addAttribute("styles", getStylesList(styleService));
        model.addAttribute("tags", getTagsList(tagService));
        model.addAttribute("breweries", getBreweryList(breweryService));

        return "beerEdit";
    }

    private Beer getVerifiedBeer(int id) {
        try {
            return beerService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Beer not found!");
        }
    }
}
