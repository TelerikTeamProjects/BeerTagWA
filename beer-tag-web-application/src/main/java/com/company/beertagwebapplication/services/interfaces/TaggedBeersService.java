package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.TaggedBeer;

public interface TaggedBeersService {
    void add(TaggedBeer taggedBeer);

    int remove(TaggedBeer taggedBeer);
}
