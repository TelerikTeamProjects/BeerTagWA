package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> get(String name);

    Style getById(int id);

    void create(Style style);

    int update(int styleId, Style style);
}
