package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.Tag;

import java.util.List;

public interface TagService {
    List<Tag> get(String name);

    Tag getById(int id);

    void create(Tag tag);

    int update(int id, Tag tag);
}
