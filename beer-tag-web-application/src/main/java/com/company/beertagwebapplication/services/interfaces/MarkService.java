package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.Mark;

import java.util.List;

public interface MarkService {
    List<Mark> get(String name);

    Mark getById(int id);

    void create(Mark mark);

    int update(int id, Mark mark);
}
