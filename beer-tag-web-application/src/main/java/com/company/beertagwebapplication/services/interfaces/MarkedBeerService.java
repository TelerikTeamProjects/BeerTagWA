package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.MarkedBeer;

import java.util.List;

public interface MarkedBeerService {
    List<MarkedBeer> get(MarkedBeer beer);

    void add(MarkedBeer markedBeer);

    int update(MarkedBeer markedBeer);

    int remove(MarkedBeer markedBeer);
}
