package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.Tag;
import com.company.beertagwebapplication.repostories.TagRepository;
import com.company.beertagwebapplication.services.interfaces.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> get(String name) {
        if (name == null) {
            return tagRepository.getAll();
        }
        return tagRepository.getByName(name);
    }

    @Override
    public Tag getById(int id) {
        Tag tag = tagRepository.getById(id);
        if (tag == null) {
            throw new IllegalArgumentException(String.format("Tag with id %d does not exist.", id));
        }
        return tag;
    }

    @Override
    public void create(Tag tag) {
        List<Tag> tags = tagRepository.getByName(tag.getName());
        if (tags.size() > 0) {
            throw new IllegalArgumentException(String.format("Tag with name %s already exists.", tag.getName()));
        }
        tagRepository.create(tag);
    }

    @Override
    public int update(int id, Tag tag) {
        Tag oldTag = tagRepository.getById(id);
        if (oldTag == null) {
            throw new IllegalArgumentException(String.format("Tag with id %d does not exist.", id));
        }

        List<Tag> tagNameExists = tagRepository.getByName(tag.getName());
        if (tagNameExists.size() > 0 && tagNameExists.get(0).getId() != id) {
            throw new IllegalArgumentException(String.format("Style with name %s already exists.", tag.getName()));
        }

        return tagRepository.update(id, tag);
    }
}
