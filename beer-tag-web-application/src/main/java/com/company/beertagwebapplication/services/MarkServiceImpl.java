package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.Mark;
import com.company.beertagwebapplication.repostories.MarkRepository;
import com.company.beertagwebapplication.services.interfaces.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class MarkServiceImpl implements MarkService {
    private MarkRepository markRepository;

    @Autowired
    public MarkServiceImpl(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }

    @Override
    public List<Mark> get(String name) {
        if (name == null || name.isEmpty()) {
            return markRepository.getAll();
        } else {
            return markRepository.getByName(name);
        }
    }

    @Override
    public Mark getById(int id) {
        Mark currentMark = markRepository.getById(id);
        if (currentMark == null) {
            throw new IllegalArgumentException(String.format("Mark with id %d not found.", id));
        }
        return currentMark;
    }

    @Override
    public void create(Mark mark) {
        List<Mark> marks = markRepository.getByName(mark.getName());
        if (marks.size() > 0) {
            throw new IllegalArgumentException(String.format("Mark with name %s already exists.", mark.getName()));
        }
        markRepository.create(mark);
    }

    @Override
    public int update(int id, Mark mark) {
        Mark currentMark = markRepository.getById(id);
        if (currentMark == null) {
            throw new IllegalArgumentException(String.format("Mark with id %d does not exist.", id));
        }

        List<Mark> markNameExists = markRepository.getByName(mark.getName());
        if (markNameExists.size() > 0 && markNameExists.get(0).getId() != id) {
            throw new IllegalArgumentException(String.format("Mark with name %s already exists.", mark.getName()));
        }

        return markRepository.update(id, mark);
    }
}
