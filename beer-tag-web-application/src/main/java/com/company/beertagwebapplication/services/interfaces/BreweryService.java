package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.Brewery;

import java.util.List;

public interface BreweryService {
    List<Brewery> get(String name);

    Brewery getById(int id);

    void create(Brewery brewery);

    int update(int id, Brewery brewery);
}
