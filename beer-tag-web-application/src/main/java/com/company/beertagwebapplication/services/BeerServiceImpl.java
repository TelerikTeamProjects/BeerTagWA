package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.Tag;
import com.company.beertagwebapplication.repostories.BeerRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.interfaces.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private UserRepository userRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, UserRepository userRepository) {
        this.beerRepository = beerRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Page<Beer> get(List<Tag> tags, String style, String country, Pageable pageable) {
        return beerRepository.get(tags, style, country, pageable);
    }

    @Override
    public Beer getById(int id) {
        Beer currentBeer = beerRepository.getById(id);
        if (currentBeer == null) {
            throw new IllegalArgumentException(String.format("Beer with id %d not found.", id));
        }
        return currentBeer;
    }

    @Override
    public Beer getByName(String name) {
        List<Beer> beers = beerRepository.getByName(name);
        if (beers.size() == 0) {
            throw new IllegalArgumentException(String.format("Beer with name %s does not exist.", name));
        }

        return beers.get(0);
    }

    @Override
    public void create(Beer beer) {
        List<Beer> currentBeer = beerRepository.getByName(beer.getName());
        if (currentBeer.size() > 0) {
            throw new IllegalArgumentException(String.format("Beer with name %s already exists.", beer.getName()));
        }

        try {
            beerRepository.create(beer);
        }catch (IllegalArgumentException e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public int update(int beerId, Beer beer, int userId) {
        Beer existingBeer = beerRepository.getById(beerId);
        if (existingBeer == null) {
            throw new IllegalArgumentException(String.format("Beer with id %d does not exists.", beer.getId()));
        }

        List<Beer> beerNameExists = beerRepository.getByName(beer.getName());
        if (beerNameExists.size() > 0 && beerNameExists.get(0).getId() != beerId) {
            throw new IllegalArgumentException(String.format("Beer with name %s already exists.", beer.getName()));
        }

        if(userRepository.getById(userId) == null){
            throw new IllegalArgumentException(String.format("User with id %d does not exist.", userId));
        }

        return beerRepository.update(beerId, beer, userId);
    }

    /*@Override
    public int updatePhotoUrl(int beerId, String photoUrl) {
        Beer beer = beerRepository.getById(beerId);
        if (beer == null) {
            throw new IllegalArgumentException(String.format("Beer with id %d does not exist.", beerId));
        }
        return beerRepository.updatePhotoUrl(beerId, photoUrl);
    }
*/
    @Override
    public int delete(int beerId, int userId) {
        Beer oldBeer = beerRepository.getById(beerId);
        if (oldBeer == null) {
            throw new IllegalArgumentException(String.format("Beer with id %d does not exist.", beerId));
        }

        if(userRepository.getById(userId) == null){
            throw new IllegalArgumentException(String.format("User with id %d does not exist.", userId));
        }

        return beerRepository.delete(beerId, userId);
    }
}
