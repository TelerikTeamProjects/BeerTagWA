package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CountryService {
    List<Country> get(String name);

    Country getById(int id);

    void create(Country country);

    int update(int id, Country country);
}
