package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.RatedBeer;
import com.company.beertagwebapplication.repostories.RatedBeerRepository;
import com.company.beertagwebapplication.services.interfaces.RatedBeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class RatedBeerServiceImpl implements RatedBeerService {
    private RatedBeerRepository ratedBeerRepository;

    @Autowired
    public RatedBeerServiceImpl(RatedBeerRepository ratedBeerRepository) {
        this.ratedBeerRepository = ratedBeerRepository;
    }

    @Override
    public List<RatedBeer> get(RatedBeer ratedBeer) {
        return ratedBeerRepository.get(ratedBeer);
    }

    @Override
    public void add(RatedBeer ratedBeer) {
        List<RatedBeer> ratedBeerList = ratedBeerRepository.get(ratedBeer);
        if (ratedBeerList.size() > 0) {
            throw new IllegalArgumentException(String.format("Beer with id %d is already rated by user with id %d.",
                    ratedBeer.getBeer().getId(), ratedBeer.getUserId()));
        }
        ratedBeerRepository.add(ratedBeer);
    }
}