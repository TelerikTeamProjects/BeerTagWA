package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.BeerImage;

public interface BeerImageService {
    BeerImage storeFile(BeerImage image, int beerId, int userId);

    BeerImage getFile(int fileId);
}
