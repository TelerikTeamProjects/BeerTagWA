package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.UserImage;

public interface UserImageService {
    UserImage storeFile(UserImage image, int userId);

    UserImage getFile(int fileId);
}
