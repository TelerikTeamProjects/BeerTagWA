package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.Country;
import com.company.beertagwebapplication.repostories.CountryRepository;
import com.company.beertagwebapplication.services.interfaces.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CountryServiceImpl implements CountryService {
    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> get(String name) {
        if (name == null || name.isEmpty()) {
            return countryRepository.getAll();
        }
        return countryRepository.getByName(name);
    }

    @Override
    public Country getById(int id) {
        Country currentCountry = countryRepository.getById(id);
        if (currentCountry == null) {
            throw new IllegalArgumentException(String.format("Country with id %d not found.", id));
        }
        return currentCountry;
    }

    @Override
    public void create(Country country) {
        List<Country> countries = countryRepository.getByName(country.getName());
        if (countries.size() > 0) {
            throw new IllegalArgumentException(String.format("Country with name %s already exists.", country.getName()));
        }
        countryRepository.create(country);
    }

    @Override
    public int update(int id, Country country) {
        Country currentCountry = countryRepository.getById(id);
        if (currentCountry == null) {
            throw new IllegalArgumentException(String.format("Country with id %d does not exist.", id));
        }

        List<Country> countryNameExists = countryRepository.getByName(country.getName());
        if (countryNameExists.size() > 0 && countryNameExists.get(0).getId() != id) {
            throw new IllegalArgumentException(String.format("Country with name %s already exists.", country.getName()));
        }

        return countryRepository.update(id, country);
    }
}
