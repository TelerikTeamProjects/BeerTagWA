package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.BeerImage;
import com.company.beertagwebapplication.repostories.BeerImageRepository;
import com.company.beertagwebapplication.repostories.BeerRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.interfaces.BeerImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class BeerImageServiceImpl implements BeerImageService {
    private BeerImageRepository beerImageRepository;
    private BeerRepository beerRepository;
    private UserRepository userRepository;

    @Autowired
    public BeerImageServiceImpl(BeerImageRepository beerImageRepository, BeerRepository beerRepository, UserRepository userRepository) {
        this.beerImageRepository = beerImageRepository;
        this.beerRepository = beerRepository;
        this.userRepository = userRepository;
    }

    public BeerImage storeFile(BeerImage image, int beerId, int userId) {
        if (beerRepository.getById(beerId) == null) {
            throw new IllegalArgumentException(String.format("Beer with id %d not found.", beerId));
        }

        if(userRepository.getById(userId) == null){
            throw new IllegalArgumentException(String.format("User with id %d does not exist.", userId));
        }

        beerImageRepository.save(image);
        beerRepository.updateImageId(beerId, image.getId(), userId);
        return image;
    }

    public BeerImage getFile(int fileId) {
        return beerImageRepository.findById(fileId)
                .orElseThrow(() -> new IllegalArgumentException("File not found with image id " + fileId));
    }
}