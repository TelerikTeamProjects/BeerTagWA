package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.UserImage;
import com.company.beertagwebapplication.repostories.UserImageRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.interfaces.UserImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class UserImageServiceImpl implements UserImageService {
    private UserImageRepository userImageRepository;
    private UserRepository userRepository;

    @Autowired
    public UserImageServiceImpl(UserImageRepository userImageRepository, UserRepository userRepository) {
        this.userImageRepository = userImageRepository;
        this.userRepository = userRepository;
    }

    public UserImage storeFile(UserImage image, int userId) {
        if (userRepository.getById(userId) == null) {
            throw new IllegalArgumentException(String.format("User with id %d not found.", userId));
        }

        userImageRepository.save(image);
        userRepository.updateImageId(userId, image.getId());
        return image;
    }

    public UserImage getFile(int fileId) {
        return userImageRepository.findById(fileId)
                .orElseThrow(() -> new IllegalArgumentException("File not found with image id " + fileId));
    }
}