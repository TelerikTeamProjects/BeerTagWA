package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.MarkedBeer;
import com.company.beertagwebapplication.models.RatedBeer;
import com.company.beertagwebapplication.models.UserDetails;

import java.util.List;

public interface UserService {
    List<UserDetails> get(String username);

    UserDetails getById(int id);

    List<RatedBeer> getRatedBeers(int id);

    List<MarkedBeer> getMarkedBeers(int id);

    void create(UserDetails userDetails);

    int update(int id, UserDetails userDetails);

    int delete(int id);
}
