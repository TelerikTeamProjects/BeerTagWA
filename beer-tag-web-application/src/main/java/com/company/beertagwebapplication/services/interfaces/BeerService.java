package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface BeerService {
    Page<Beer> get(List<Tag> tags, String style, String country, Pageable pageable);

    Beer getById(int id);

    Beer getByName(String name);

    void create(Beer beer);

    int update(int beerId, Beer beer, int userID);

    int delete(int beerId, int userId);
}
