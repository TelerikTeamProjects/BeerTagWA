package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.MarkedBeer;
import com.company.beertagwebapplication.repostories.MarkedBeerRepository;
import com.company.beertagwebapplication.services.interfaces.MarkedBeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class MarkedBeerServiceImpl implements MarkedBeerService {
    private MarkedBeerRepository markedBeerRepository;

    @Autowired
    public MarkedBeerServiceImpl(MarkedBeerRepository markedBeerRepository) {
        this.markedBeerRepository = markedBeerRepository;
    }

    @Override
    public List<MarkedBeer> get(MarkedBeer markedBeer) {
        return markedBeerRepository.get(markedBeer);
    }

    @Override
    public void add(MarkedBeer markedBeer) {
        List<MarkedBeer> markedBeerList = markedBeerRepository.get(markedBeer);
        if (markedBeerList.size() > 0) {
            throw new IllegalArgumentException(String.format("Beer with id %d is already marked by user with id %d.",
                    markedBeer.getBeer().getId(), markedBeer.getUserId()));
        }
        markedBeerRepository.add(markedBeer);
    }

    @Override
    public int update(MarkedBeer markedBeer) {
        List<MarkedBeer> markedBeerList = markedBeerRepository.get(markedBeer);
        if (markedBeerList.size() != 1) {
            throw new IllegalArgumentException(String.format("Beer with id %d does not exist.",
                    markedBeer.getBeer().getId()));
        }
        return markedBeerRepository.update(markedBeer);
    }

    @Override
    public int remove(MarkedBeer markedBeer) {
        List<MarkedBeer> markedBeerList = markedBeerRepository.get(markedBeer);
        if (markedBeerList.size() == 0) {
            throw new IllegalArgumentException(String.format("Beer with id %d does not exist.",
                    markedBeer.getBeer().getId()));
        }

        markedBeer.setId(markedBeerList.get(0).getId());
        return markedBeerRepository.remove(markedBeer);
    }
}