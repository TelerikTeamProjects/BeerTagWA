package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.MarkedBeer;
import com.company.beertagwebapplication.models.RatedBeer;
import com.company.beertagwebapplication.models.UserDetails;
import com.company.beertagwebapplication.repostories.MarkedBeerRepository;
import com.company.beertagwebapplication.repostories.RatedBeerRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private RatedBeerRepository ratedBeerRepository;
    private MarkedBeerRepository markedBeerRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RatedBeerRepository ratedBeerRepository, MarkedBeerRepository markedBeerRepository) {
        this.userRepository = userRepository;
        this.ratedBeerRepository = ratedBeerRepository;
        this.markedBeerRepository = markedBeerRepository;
    }

    @Override
    public List<UserDetails> get(String username) {
        if (username == null || username.isEmpty()) {
            return userRepository.getAll();
        } else {
            return userRepository.getByName(username);
        }
    }

    @Override
    public UserDetails getById(int id) {
        validateId(id);
        return userRepository.getById(id);
    }

    @Override
    public List<RatedBeer> getRatedBeers(int id) {
        validateId(id);

        return ratedBeerRepository.topRatedBeersByUserId(id);
    }

    @Override
    public List<MarkedBeer> getMarkedBeers(int id) {
        validateId(id);

        return markedBeerRepository.markedBeersByUserId(id);
    }

    @Override
    public void create(UserDetails userDetails) {
        List<UserDetails> userDetailsExists = userRepository.getByName(userDetails.getUsername());
        if (userDetailsExists.size() != 0) {
            throw new IllegalArgumentException(String.format("User with username %s already exists.", userDetails.getUsername()));
        }
        userRepository.create(userDetails);
}
    @Override
    public int update(int id, UserDetails userDetails) {
        validateId(id);

        return userRepository.update(id, userDetails);
    }

    @Override
    public int delete(int id) {
        validateId(id);

        return userRepository.delete(id);
    }

    private UserDetails validateId(int id) {
        UserDetails userDetailsById = userRepository.getById(id);
        if (userDetailsById == null || !userDetailsById.isActive()) {
            throw new IllegalArgumentException(String.format("User with id %d does not exist.", id));
        }
        return userDetailsById;
    }
}
