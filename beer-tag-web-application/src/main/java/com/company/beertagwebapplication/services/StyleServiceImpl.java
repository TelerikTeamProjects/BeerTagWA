package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.Style;
import com.company.beertagwebapplication.repostories.StyleRepository;
import com.company.beertagwebapplication.services.interfaces.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> get(String name) {
        if (name == null) {
            return styleRepository.getAll();
        }
        return styleRepository.getByName(name);
    }

    @Override
    public Style getById(int id) {
        Style style = styleRepository.getById(id);
        if (style == null) {
            throw new IllegalArgumentException(String.format("Style with id %d does not exist."));
        }
        return style;
    }

    @Override
    public void create(Style style) {
        List<Style> styles = styleRepository.getByName(style.getName());
        if (styles.size() > 0) {
            throw new IllegalArgumentException(String.format("Style with name %s already exists.", style.getName()));
        }
        styleRepository.create(style);
    }

    @Override
    public int update(int id, Style style) {
        Style oldStyle = styleRepository.getById(id);
        if (oldStyle == null) {
            throw new IllegalArgumentException(String.format("Style with id %d does not exist.", id));
        }

        List<Style> styleNameExists = styleRepository.getByName(style.getName());
        if (styleNameExists.size() > 0 && styleNameExists.get(0).getId() != id) {
            throw new IllegalArgumentException(String.format("Style with name %s already exists.", style.getName()));
        }

        return styleRepository.update(id, style);
    }
}
