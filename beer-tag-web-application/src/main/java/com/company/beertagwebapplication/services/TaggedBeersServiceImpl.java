package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.TaggedBeer;
import com.company.beertagwebapplication.repostories.TaggedBeerRepository;
import com.company.beertagwebapplication.services.interfaces.TaggedBeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class TaggedBeersServiceImpl implements TaggedBeersService {
    private TaggedBeerRepository beerTagsRepository;

    @Autowired
    public TaggedBeersServiceImpl(TaggedBeerRepository beerTagsRepository) {
        this.beerTagsRepository = beerTagsRepository;
    }

    @Override
    public void add(TaggedBeer taggedBeer) {
        List<TaggedBeer> taggedBeers = beerTagsRepository.get(taggedBeer);
        if (taggedBeers.size() > 0) {
            throw new IllegalArgumentException(String.format("Beer wih id %d is already tagged with tag with id %d.",
                    taggedBeer.getBeerId(), taggedBeer.getTagId()));
        }
        beerTagsRepository.add(taggedBeer);
    }

    @Override
    public int remove(TaggedBeer taggedBeer) {
        List<TaggedBeer> taggedBeers = beerTagsRepository.get(taggedBeer);
        if (taggedBeers.size() == 0) {
            throw new IllegalArgumentException(String.format("Beer wih id %d and tag with id %d does not exist.",
                    taggedBeer.getBeerId(), taggedBeer.getTagId()));
        }
        taggedBeer.setId(taggedBeers.get(0).getId());
        return beerTagsRepository.remove(taggedBeer);
    }
}
