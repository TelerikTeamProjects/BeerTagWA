package com.company.beertagwebapplication.services.interfaces;

import com.company.beertagwebapplication.models.RatedBeer;

import java.util.List;

public interface RatedBeerService {
    List<RatedBeer> get(RatedBeer beer);

    void add(RatedBeer ratedBeer);
}
