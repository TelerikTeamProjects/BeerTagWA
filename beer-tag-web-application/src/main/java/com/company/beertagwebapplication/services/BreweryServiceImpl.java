package com.company.beertagwebapplication.services;

import com.company.beertagwebapplication.models.Brewery;
import com.company.beertagwebapplication.repostories.BreweryRepository;
import com.company.beertagwebapplication.services.interfaces.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public List<Brewery> get(String name) {
        if (name == null || name.isEmpty()) {
            return breweryRepository.getAll();
        }
        return breweryRepository.getByName(name);
    }

    @Override
    public Brewery getById(int id) {
        Brewery currentBrewery = breweryRepository.getById(id);
        if (currentBrewery == null) {
            throw new IllegalArgumentException(String.format("Brewery with id %d not found.", id));
        }
        return currentBrewery;
    }

    @Override
    public void create(Brewery brewery) {
        List<Brewery> breweries = breweryRepository.getByName(brewery.getName());
        if (breweries.size() > 0) {
            throw new IllegalArgumentException(String.format("Brewery with name %s already exists.", brewery.getName()));
        }
        breweryRepository.create(brewery);
    }

    @Override
    public int update(int id, Brewery brewery) {
        Brewery currentBrewery = breweryRepository.getById(id);
        if (currentBrewery == null) {
            throw new IllegalArgumentException(String.format("Brewery with id %d does not exist.", id));
        }

        List<Brewery> breweryNameExists = breweryRepository.getByName(brewery.getName());
        if (breweryNameExists.size() > 0 && breweryNameExists.get(0).getId() != id) {
            throw new IllegalArgumentException(String.format("Brewery with name %s already exists.", brewery.getName()));
        }

        return breweryRepository.update(id, brewery);
    }
}