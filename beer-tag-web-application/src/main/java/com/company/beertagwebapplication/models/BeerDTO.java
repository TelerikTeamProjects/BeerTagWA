package com.company.beertagwebapplication.models;

public class BeerDTO {
    private String name;
    private int breweryId;
    private int countryId;
    private double abv;
    private String description;
    private int styleId;

    public BeerDTO() {
    }

    public BeerDTO(String name, int breweryId, int countryId, double abv, String description, int styleId) {
        this.name = name;
        this.breweryId = breweryId;
        this.countryId = countryId;
        this.abv = abv;
        this.description = description;
        this.styleId = styleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }
}
