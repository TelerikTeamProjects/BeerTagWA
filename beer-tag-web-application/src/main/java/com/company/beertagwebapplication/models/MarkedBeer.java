package com.company.beertagwebapplication.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "marked_beers")
public class MarkedBeer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "marked_beer_id")
    @NotNull
    private int id;

    @ManyToOne
    @JoinColumn(name = "beer_id")
    @NotNull
    private Beer beer;

    @Column(name = "user_id")
    @NotNull
    private int userId;

    @Column(name = "mark_id")
    @NotNull
    private int markId;

    public MarkedBeer() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Beer getBeer() {
        return beer;
    }

    public MarkedBeer(int id, Beer beer, int userId, int markId) {
        this.id = id;
        this.beer = beer;
        this.userId = userId;
        this.markId = markId;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMarkId() {
        return markId;
    }

    public void setMarkId(int markId) {
        this.markId = markId;
    }
}
