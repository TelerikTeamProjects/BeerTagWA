package com.company.beertagwebapplication.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "beers")
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    @NotNull
    private int id;

    @Column(name = "beer_name")
    @NotNull
    @Size(min = 3, max = 50, message = "Beer name should be between 3 and 50 characters.")
    private String name;

    @OneToOne
    @JoinColumn(name = "brewery_id")
    @NotNull
    private Brewery brewery;

    @OneToOne
    @JoinColumn(name = "country_id")
    @NotNull
    private Country country;

    @Column(name = "ABV")
    @NotNull
    private double abv;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "style_id")
    @NotNull
    private Style style;

    @Column(name = "avg_rating")
    @NotNull
    private double avgRating;

    @ManyToOne
    @JoinColumn(name = "owner")
    @JsonIgnore
    private UserDetails owner;

    @Column(name = "is_active")
    private Boolean isActive;

    @ManyToOne
    @JoinColumn(name = "modified_by")
    @JsonIgnore
    private UserDetails modifiedBy;

    @OneToOne
    @JoinColumn(name = "image_id")
    private BeerImage image;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beer_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    @JsonIgnore
    private List<Tag> tags;


    public Beer() {
    }

    public Beer(int id, String name, Brewery brewery, Country country, double abv, String description, Style style, double avgRating, UserDetails owner, Boolean isActive, UserDetails modifiedBy, BeerImage image) {
        this.id = id;
        this.name = name;
        this.brewery = brewery;
        this.country = country;
        this.abv = abv;
        this.description = description;
        this.style = style;
        this.avgRating = avgRating;
        this.owner = owner;
        this.isActive = isActive;
        this.modifiedBy = modifiedBy;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public UserDetails getOwner() {
        return owner;
    }

    public void setOwner(UserDetails owner) {
        this.owner = owner;
    }

    public UserDetails getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(UserDetails modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public Boolean isActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<String> getTagNames() {
        if (tags != null) {
            return tags.stream().map(Tag::getName).collect(Collectors.toList());
        }
        return null;
    }

    public BeerImage getImage() {
        return image;
    }

    public void setImage(BeerImage image) {
        this.image = image;
    }
}
