package com.company.beertagwebapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "breweries")
public class Brewery {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brewery_id")
    @NotNull
    private int id;

    @Column(name = "brewery_name")
    @NotNull
    @Size(min = 3, max = 50, message = "Brewery name should be between 3 and 50 characters.")
    private String name;

    public Brewery() {
    }

    public Brewery(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
