package com.company.beertagwebapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_images")
public class UserImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    @NotNull
    private int id;

    @Column(name = "user_id")
    @NotNull
    private int userId;

    @Column(name = "image_name")
    @NotNull
    private String fileName;


    @Column(name = "type")
    @NotNull
    private String fileType;

    @Column(name = "image")
    private byte[] image;

    public UserImage() {
    }

    public UserImage(int userId, String fileName, String fileType, byte[] image) {
        this.userId = userId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image;
    }

    public UserImage(int id, int userId, String fileName, String fileType, byte[] image) {
        this.id = id;
        this.userId = userId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
