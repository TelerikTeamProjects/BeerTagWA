package com.company.beertagwebapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "beer_tags")
public class TaggedBeer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_tag_id")
    private int id;

    @Column(name = "beer_id")
    @NotNull
    private int beerId;

    @Column(name = "tag_id")
    @NotNull
    private int tagId;

    @Column(name = "user_id")
    @NotNull
    private int userId;

    public TaggedBeer() {}

    public TaggedBeer(int id, int beerId, int tagId, int userId) {
        this.beerId = beerId;
        this.tagId = tagId;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
