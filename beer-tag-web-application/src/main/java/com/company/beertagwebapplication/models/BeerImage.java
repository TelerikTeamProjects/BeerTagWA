package com.company.beertagwebapplication.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "beer_images")
public class BeerImage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_id")
    @NotNull
    private int id;

    @Column(name = "beer_id")
    @NotNull
    private int beerId;

    @Column(name = "image_name")
    @NotNull
    private String fileName;


    @Column(name = "type")
    @NotNull
    private String fileType;

    @Column(name = "image")
    private byte[] image;

    public BeerImage() {
    }

    public BeerImage(int beerId, String fileName, String fileType, byte[] image) {
        this.beerId = beerId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image;
    }

    public BeerImage(int id, int beerId, String fileName, String fileType, byte[] image) {
        this.id = id;
        this.beerId = beerId;
        this.fileName = fileName;
        this.fileType = fileType;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}