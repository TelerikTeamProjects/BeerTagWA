package com.company.beertagwebapplication.models;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "rated_beers")
public class RatedBeer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_rating_id")
    @NotNull
    private int id;

    @Column(name = "beer_rating")
    @NotNull
    @Min(1)
    @Max(5)
    private int beerRating;

    @ManyToOne
    @JoinColumn(name = "beer_id")
    @NotNull
    private Beer beer;

    @Column(name = "user_id")
    @NotNull
    private int userId;

    public RatedBeer() {
    }

    public RatedBeer(int id, int beerRating, Beer beer, int userId) {
        this.id = id;
        this.beerRating = beerRating;
        this.beer = beer;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBeerRating() {
        return beerRating;
    }

    public void setBeerRating(int beerRating) {
        this.beerRating = beerRating;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
