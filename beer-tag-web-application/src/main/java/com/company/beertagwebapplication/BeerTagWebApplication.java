package com.company.beertagwebapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeerTagWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeerTagWebApplication.class, args);
    }
}