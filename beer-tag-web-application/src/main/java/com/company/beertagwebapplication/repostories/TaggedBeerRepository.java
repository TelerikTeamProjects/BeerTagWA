package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.TaggedBeer;

import java.util.List;

public interface TaggedBeerRepository {
    List<TaggedBeer> get(TaggedBeer taggedBeer);

    void add(TaggedBeer beerTag);

    int remove(TaggedBeer beerTag);
}
