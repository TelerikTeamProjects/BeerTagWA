package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.BeerImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeerImageRepository extends JpaRepository<BeerImage, Integer> {
}
