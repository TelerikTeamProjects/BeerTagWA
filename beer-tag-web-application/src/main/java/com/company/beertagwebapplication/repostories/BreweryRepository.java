package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Brewery;

import java.util.List;

public interface BreweryRepository {
    List<Brewery> getAll();

    List<Brewery> getByName(String name);

    Brewery getById(int id);

    void create(Brewery brewery);

    int update(int id, Brewery brewery);
}
