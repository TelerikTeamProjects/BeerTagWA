package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Page<Beer> get(List<Tag> tags, String style, String country, Pageable pageable) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Beer> criteriaQuery = builder.createQuery(Beer.class);
        Root<Beer> root = criteriaQuery.from(Beer.class);

        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder.equal(root.get("isActive"), true));
        if (style != null) {
            predicates.add(builder.equal(root.get("style").get("name"), style));
        }
        if (country != null) {
            predicates.add(builder.equal(root.get("country").get("name"), country));
        }
        if (tags != null && tags.size() > 0) {
            Expression<Collection<Tag>> beerTags = root.get("tags");

            List<Predicate> subPredicates = new ArrayList<>();
            for (Tag tag : tags) {
                subPredicates.add(builder.or(builder.isMember(tag, beerTags)));
            }
            Predicate subPredicate = builder.or(subPredicates.toArray(new Predicate[]{}));

            predicates.add(subPredicate);
        }

        List<Order> orderBy = new ArrayList<>();
        if (pageable.getSort().isSorted()) {
            if (pageable.getSort().getOrderFor("abv") != null) {
                if (pageable.getSort().getOrderFor("abv").getDirection().toString().equals("ASC")) {
                    orderBy.add(builder.asc(root.get("abv")));
                } else {
                    orderBy.add(builder.desc(root.get("abv")));
                }
                orderBy.add(builder.asc(root.get("name")));
            }
            if (pageable.getSort().getOrderFor("avgRating") != null) {
                if (pageable.getSort().getOrderFor("avgRating").getDirection().toString().equals("ASC")) {
                    orderBy.add(builder.asc(root.get("avgRating")));
                } else {
                    orderBy.add(builder.desc(root.get("avgRating")));
                }
                orderBy.add(builder.asc(root.get("name")));
            }
            if (pageable.getSort().getOrderFor("name") != null) {
                if (pageable.getSort().getOrderFor("name").getDirection().toString().equals("ASC")) {
                    orderBy.add(builder.asc(root.get("name")));
                } else {
                    orderBy.add(builder.desc(root.get("name")));
                }
            }
        }

        criteriaQuery.select(root).where(predicates.toArray(new Predicate[]{})).orderBy(orderBy);
        TypedQuery<Beer> result = session.createQuery(criteriaQuery);

        int totalRows = result.getResultList().size();
        result.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        result.setMaxResults(pageable.getPageSize());
        
        return new PageImpl<>(result.getResultList(), pageable, totalRows);
    }

    public List<Beer> getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where beer_name = :beerName and is_active = true", Beer.class);
        query.setParameter("beerName", name);
        return query.list();
    }

    @Override
    public Beer getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where beer_id = :beerId and is_active = true", Beer.class);
        query.setParameter("beerId", id);
        List<Beer> result = query.list();
        return result.size() > 0 ? result.get(0) : null;
    }

    @Override
    public void create(Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.save(beer);
        }catch (Exception e){
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public int update(int beerId, Beer beer, int userId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Beer set beer_name = :name, brewery_id = :brewery_id, " +
                "country_id = :country_id, ABV=:abv, description = :description, style_id = :style_id," +
                "modified_by = :modified_by where beer_id = :id");
        query.setParameter("id", beerId);
        query.setParameter("name", beer.getName());
        query.setParameter("brewery_id", beer.getBrewery().getId());
        query.setParameter("country_id", beer.getCountry().getId());
        query.setParameter("abv", beer.getAbv());
        query.setParameter("description", beer.getDescription());
        query.setParameter("style_id", beer.getStyle().getId());
        query.setParameter("modified_by", userId);
        return query.executeUpdate();
    }

    @Override
    public int updateImageId(int beerId, int imageId, int userId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Beer set image_id = :imageId, modified_by = :modified_by where beer_id = :id");
        query.setParameter("id", beerId);
        query.setParameter("imageId", imageId);
        query.setParameter("modified_by", userId);
        return query.executeUpdate();
    }

    @Override
    public int delete(int beerId, int userId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Beer set is_active = :is_active, " +
                "modified_by = :modified_by where beer_id = :id");
        query.setParameter("id", beerId);
        query.setParameter("is_active", 0);
        query.setParameter("modified_by", userId);
        return query.executeUpdate();
    }
}
