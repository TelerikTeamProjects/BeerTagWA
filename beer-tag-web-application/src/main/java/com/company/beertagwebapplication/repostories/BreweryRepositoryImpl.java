package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Brewery> query = session.createQuery("from Brewery", Brewery.class);
        return query.list();
    }

    @Override
    public List<Brewery> getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Brewery> query = session.createQuery("from Brewery where brewery_name = :name", Brewery.class);
        query.setParameter("name", name);
        return query.list();
    }

    @Override
    public Brewery getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Brewery.class, id);
    }

    @Override
    public void create(Brewery brewery) {
        Session session = sessionFactory.getCurrentSession();
        session.save(brewery);
    }

    @Override
    public int update(int id, Brewery brewery) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "update Brewery set brewery_name = :name where brewery_id = :id");
        query.setParameter("id", id);
        query.setParameter("name", brewery.getName());
        int result = query.executeUpdate();
        return result;
    }
}

