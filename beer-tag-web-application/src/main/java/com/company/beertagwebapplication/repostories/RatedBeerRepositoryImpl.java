package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.RatedBeer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RatedBeerRepositoryImpl implements RatedBeerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public RatedBeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<RatedBeer> get(RatedBeer ratedBeer) {
        Session session = sessionFactory.getCurrentSession();
        Query<RatedBeer> query = session.createQuery("from RatedBeer where beer_id = :beerId and user_id = :userId ", RatedBeer.class);
        query.setParameter("beerId", ratedBeer.getBeer().getId());
        query.setParameter("userId", ratedBeer.getUserId());

        return query.list();
    }

    @Override
    public List<RatedBeer> topRatedBeersByUserId(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query<RatedBeer> query = session.createQuery("from RatedBeer where user_id = :userId order by beer_rating desc", RatedBeer.class);
        query.setParameter("userId", id);

        return query.setMaxResults(3).list();
    }

    @Override
    public void add(RatedBeer ratedBeer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(ratedBeer);
    }
}
