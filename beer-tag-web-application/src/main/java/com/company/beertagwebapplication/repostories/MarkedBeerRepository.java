package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.MarkedBeer;

import java.util.List;

public interface MarkedBeerRepository {
    List<MarkedBeer> get(MarkedBeer markedBeer);

    List<MarkedBeer> markedBeersByUserId(int id);

    void add(MarkedBeer markedBeer);

    int update(MarkedBeer markedBeer);

    int remove(MarkedBeer markedBeer);
}
