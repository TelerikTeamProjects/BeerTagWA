package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserImageRepository extends JpaRepository<UserImage, Integer> {
}
