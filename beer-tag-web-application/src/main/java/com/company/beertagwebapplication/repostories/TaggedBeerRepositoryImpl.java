package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.TaggedBeer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaggedBeerRepositoryImpl implements TaggedBeerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public TaggedBeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<TaggedBeer> get(TaggedBeer taggedBeer) {
        Session session = sessionFactory.getCurrentSession();
        Query<TaggedBeer> query = session.createQuery("from TaggedBeer where beer_id = :beerId and tag_id = :tagId", TaggedBeer.class);
        query.setParameter("beerId", taggedBeer.getBeerId());
        query.setParameter("tagId", taggedBeer.getTagId());

        return query.list();
    }

    @Override
    public void add(TaggedBeer taggedBeer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(taggedBeer);
    }

    @Override
    public int remove(TaggedBeer taggedBeer) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from TaggedBeer where beer_tag_id = :beer_tag_id");
        query.setParameter("beer_tag_id", taggedBeer.getId());
        return query.executeUpdate();
    }
}
