package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Beer;
import com.company.beertagwebapplication.models.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface BeerRepository {
    Page<Beer> get(List<Tag> tags, String style, String country, Pageable pageable);

    List<Beer> getByName(String name);

    Beer getById(int id);

    void create(Beer beer);

    int update(int beerId, Beer beer, int userId);

    int updateImageId(int beerId, int imageId, int userId);

    int delete(int beerId, int userId);
}
