package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.UserDetails;


import java.util.List;

public interface UserRepository {
    List<UserDetails> getAll();

    List<UserDetails> getByName(String username);

    UserDetails getById(int id);

    void create(UserDetails user);

    int update(int id, UserDetails userDetails);

    int updateImageId(int userId, int imageId);

    int delete(int id);
}
