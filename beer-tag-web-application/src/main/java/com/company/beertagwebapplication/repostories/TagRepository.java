package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();

    List<Tag> getByName(String name);

    Tag getById(int id);

    void create(Tag tag);

    int update(int tagId, Tag tag);
}
