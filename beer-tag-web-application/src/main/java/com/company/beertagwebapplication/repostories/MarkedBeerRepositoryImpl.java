package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.MarkedBeer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MarkedBeerRepositoryImpl implements MarkedBeerRepository {
    SessionFactory sessionFactory;

    @Autowired
    public MarkedBeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<MarkedBeer> get(MarkedBeer markedBeer) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from MarkedBeer where beer_id = :beerId and user_id = :userId and mark_id = :markId", MarkedBeer.class);
        query.setParameter("beerId", markedBeer.getBeer().getId());
        query.setParameter("userId", markedBeer.getUserId());
        query.setParameter("markId", markedBeer.getMarkId());

        return query.list();
    }

    @Override
    public List<MarkedBeer> markedBeersByUserId(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from MarkedBeer where user_id = :userId and mark_id = 1", MarkedBeer.class);
        query.setParameter("userId", id);

        return query.list();
    }

    @Override
    public void add(MarkedBeer markedBeer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(markedBeer);
    }

    @Override
    public int update(MarkedBeer markedBeer) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update MarkedBeer set mark_id = :markId where marked_beer_id = :markeBeerId");
        query.setParameter("markId", markedBeer.getMarkId());
        query.setParameter("markeBeerId", markedBeer.getId());
        return query.executeUpdate();
    }

    @Override
    public int remove(MarkedBeer markedBeer) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from MarkedBeer where marked_beer_id = :markeBeerId");
        query.setParameter("markeBeerId", markedBeer.getId());
        return query.executeUpdate();
    }
}
