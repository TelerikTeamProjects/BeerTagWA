package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> query = session.createQuery("from Tag", Tag.class);
        return query.list();
    }

    @Override
    public List<Tag> getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> query = session.createQuery("from Tag where tag_name = :name", Tag.class);
        query.setParameter("name", name);
        return query.list();
    }

    @Override
    public Tag getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Tag.class, id);
    }

    @Override
    public void create(Tag tag) {
        Session session = sessionFactory.getCurrentSession();
        session.save(tag);
    }

    @Override
    public int update(int id, Tag tag) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Tag set tag_name = :name where tag_id = :id");
        query.setParameter("id", id);
        query.setParameter("name", tag.getName());
        return query.executeUpdate();
    }

    //TODO delete tag

}
