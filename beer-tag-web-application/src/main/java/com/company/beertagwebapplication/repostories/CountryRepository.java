package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Country;

import java.util.List;

public interface CountryRepository{
    List<Country> getAll();

    List<Country> getByName(String name);

    Country getById(int id);

    void create(Country country);

    int update(int id, Country country);
}
