package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.RatedBeer;

import java.util.List;

public interface RatedBeerRepository {
    List<RatedBeer> get(RatedBeer ratedBeer);

    List<RatedBeer> topRatedBeersByUserId(int id);

    void add(RatedBeer ratedBeer);
}
