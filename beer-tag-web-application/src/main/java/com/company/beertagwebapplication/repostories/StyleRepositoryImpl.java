package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style", Style.class);
        return query.list();
    }

    @Override
    public List<Style> getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style where style_name = :name", Style.class);
        query.setParameter("name", name);
        return query.list();
    }

    @Override
    public Style getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Style.class, id);
    }

    @Override
    public void create(Style style) {
        Session session = sessionFactory.getCurrentSession();
        session.save(style);
    }

    @Override
    public int update(int id, Style style) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update Style set style_name = :name where style_id = :id");
        query.setParameter("id", id);
        query.setParameter("name", style.getName());
        return query.executeUpdate();
    }
}
