package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Mark;

import java.util.List;

public interface MarkRepository {
    List<Mark> getAll();

    List<Mark> getByName(String name);

    Mark getById(int id);

    void create(Mark mark);

    int update(int id, Mark mark);
}
