package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Style;

import java.util.List;

public interface StyleRepository {
    List<Style> getAll();

    List<Style> getByName(String name);

    Style getById(int id);

    void create(Style style);

    int update(int id, Style style);
}
