package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;

    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country", Country.class);
        return query.list();
    }

    @Override
    public List<Country> getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country where country_name = :countryName", Country.class);
        query.setParameter("countryName", name);
        return query.list();
    }

    @Override
    public Country getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Country.class, id);
    }

    @Override
    public void create(Country country) {
        Session session = sessionFactory.getCurrentSession();
        session.save(country);
    }

    @Override
    public int update(int id, Country country) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "update Country set country_name = :name where country_id = :id");
        query.setParameter("id", id);
        query.setParameter("name", country.getName());
        int result = query.executeUpdate();
        return result;
    }
}
