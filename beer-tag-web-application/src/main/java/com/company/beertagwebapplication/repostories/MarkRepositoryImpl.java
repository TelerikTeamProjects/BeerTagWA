package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.Mark;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MarkRepositoryImpl implements MarkRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public MarkRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Mark> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Mark> query = session.createQuery("from Mark", Mark.class);
        return query.list();
    }

    @Override
    public List<Mark> getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Mark> query = session.createQuery("from Mark where mark_name = :name", Mark.class);
        query.setParameter("name", name);
        return query.list();
    }

    @Override
    public Mark getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Mark.class, id);
    }

    @Override
    public void create(Mark mark) {
        Session session = sessionFactory.getCurrentSession();
        session.save(mark);
    }

    @Override
    public int update(int id, Mark mark) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "update Mark set mark_name = :name where mark_id = :id");
        query.setParameter("id", id);
        query.setParameter("name", mark.getName());
        int result = query.executeUpdate();
        return result;
    }
}
