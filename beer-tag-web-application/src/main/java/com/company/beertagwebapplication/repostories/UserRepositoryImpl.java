package com.company.beertagwebapplication.repostories;

import com.company.beertagwebapplication.models.UserDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserDetails> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<UserDetails> query = session.createQuery("from UserDetails where enabled = true", UserDetails.class);
        return query.list();
    }

    @Override
    public List<UserDetails> getByName(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query<UserDetails> query = session.createQuery("from UserDetails where username = :username and enabled = true", UserDetails.class);
        query.setParameter("username", username);
        return query.list();
    }

    @Override
    public UserDetails getById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(UserDetails.class, id);
    }

    @Override
    public void create(UserDetails user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Override
    public int update(int id, UserDetails userDetails) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "update UserDetails set first_name = :firstName, middle_name =  :middleName, last_name = :lastName where user_id = :id ");
        query.setParameter("id", id);
        query.setParameter("firstName", userDetails.getFirstName());
        query.setParameter("middleName", userDetails.getMiddleName());
        query.setParameter("lastName", userDetails.getLastName());
        int result = query.executeUpdate();
        return result;
    }

    @Override
    public int updateImageId(int userId, int imageId) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("update UserDetails set image_id = :imageId where user_id = :id");
        query.setParameter("id", userId);
        query.setParameter("imageId", imageId);
        return query.executeUpdate();
    }

    @Override
    public int delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "update UserDetails set enabled = 0 where user_id = :id ");
        query.setParameter("id", id);
        int result = query.executeUpdate();
        return result;
    }
}
