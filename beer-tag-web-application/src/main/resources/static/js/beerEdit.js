'use strict';

$(document).ready(function () {
    $('select').selectpicker();

    let readURL = function (input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();

            reader.onload = function (e) {
                $('.beer-image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-input").on('change', function () {
        readURL(this);
    });

    $(document).on('click', '#main_reset', function () {
        $("#style").selectpicker('val', styleId);
        $("#country").selectpicker('val', countryId);
        $("#brewery").selectpicker('val', breweryId);
    });
});

$('#singleUploadForm').submit(function (event) {
    let formElement = this;
    let formData = new FormData(formElement);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/api/beerimages/upload/" + beerId,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            singleFileUploadError.style.display = "none";
            singleFileUploadSuccess.innerHTML = "<p>File Uploaded Successfully.</p>"; /*+
                "<p>DownloadUrl : <a href='" + response.fileDownloadUri + "' target='_blank'>" + response.fileDownloadUri + "</a></p>";*/
            singleFileUploadSuccess.style.display = "block";
        },
        error: function (error) {
            singleFileUploadSuccess.style.display = "none";
            singleFileUploadError.innerHTML = "<p>Some Error Occurred</p>"
            singleFileUploadError.style.display = "block";
        }
    });

    event.preventDefault();
});

$('#beerUpdateForm').submit(function (event) {
    $('#update_beer').hide();
    $('#update_beer_spin').attr('hidden', false);

    let name = $('#beer_name').val();
    let abv = $('#beer_abv').val();
    let styleId = $('#style').val();
    let countryId = $('#country').val();
    let breweryId = $('#brewery').val();
    let description = $('#description_text').val();

    let jsonString = JSON.stringify({
        name: name,
        brewery: {
            id: breweryId
        },
        country: {
            id: countryId
        },
        abv: abv,
        description: description,
        style: {
            id: styleId
        }
    });

    $.ajax({
        type: "PUT",
        url: "/api/beers/" + beerId,
        data: jsonString,
        contentType: 'application/json',
        success: function (response) {
            // location.reload();
            window.location.href = "/beers/" + beerId;
        }
    });
});

function deleteBeer() {
    $('#deleteBtn').hide();
    $('#delete_beer_spin').attr('hidden', false);

    $.ajax({
        type: "DELETE",
        url: "/api/beers/" + beerId,
        success: function () {
            window.location.href = "/";
        }
    });
}
