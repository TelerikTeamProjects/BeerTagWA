'use strict';

loadBeers();

$(document).ready(function () {
    $('select').selectpicker();

    $(document).on('click', 'a.page-link', function () {
        if (!$(this).parent().hasClass('active')) {
            loadBeers($(this).data('value'))
        }
    });

    $(document).ajaxStart(function () {
        $('#spinner').show();
    }).ajaxStop(function () {
        $('#spinner').hide();
    });

    $(document).on('click', 'a.beer-link', function () {
        $('#spinner').show();
    });
});

function clearFilters() {
    $('#tags-select').val('').trigger('change');
    $('#country').val('').trigger('change');
    $('#style').val('').trigger('change');
    $('#orderBy').val('').trigger('change');
    loadBeers();
}

function loadBeers(page) {
    let style = $('#style option:selected').val();
    let country = $('#country option:selected').val();
    let tags = $('#tags-select').val().join('%23');
    let orderBy = $('#orderBy option:selected').val();

    if (page === undefined) {
        page = 0;
    } else {
        page = Number(page) - 1;
    }

    let $url = generateUrl(style, country, tags, page, orderBy);
    $.ajax({
        url: $url,
        success: function (response) {
            $('.beer-container').html("");
            if (response.content.length === 0) {
                $('.beer-container').append(`<h2 id="tableTitle" th:text="#{beers.title}">No beers found</h2>`);
                $('.pagination').html("");

            } else {
                $('.pagination').html("");
                $('.pagination').append(`<li class="page-item prev"><a data-value="${response.pageable.pageNumber}" class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                        </a></li>`);

                for (let i = 1; i <= response.totalPages; i++) {
                    $('.pagination').append(`<li class="page-item page-item-${i}"><a data-value="${i}" class="page-link" href="#">${i}</a></li>`);
                    if (i === response.pageable.pageNumber + 1) {
                        $('.page-item-' + String(i)).addClass('active');
                    }
                }

                $('.pagination').append(`<li class="page-item next"><a data-value="${response.pageable.pageNumber + 2}" class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                        </a></li>`);

                if (response.pageable.pageNumber === 0) {
                    $('.prev').addClass('disabled');
                }
                if (response.pageable.pageNumber + 1 === response.totalPages) {
                    $('.next').addClass('disabled');
                }

                $('.beer-container').append(`<h2 id="tableTitle" th:text="#{beers.title}">Beers</h2>`);

                let counter = 0;
                let rowNumber = 0;
                $.each(response.content, function (i, v) {
                    let beerRowId = "beerRow" + String(rowNumber);
                    if (counter % 4 === 0) {
                        rowNumber += 1;
                        beerRowId = "beerRow" + String(rowNumber);
                        $('.beer-container').append(`<div id="${beerRowId}" class="row beer-row"></div>`);
                    }
                    let imageVar;
                    if (response.content[i].image !== null) {
                        imageVar = 'data:image/png;base64,' + response.content[i].image.image;
                    } else {
                        imageVar = '/images/beer_default_image.jpg';
                    }
                    $('#' + beerRowId).append(`
                <div class="col-md-3 beer-col">
                    <a class="beer-link" href="/beers/${response.content[i].id}">
                        <div class="beer-holder">
                        <div class="image holder text-center">
                            <img src="${imageVar}" class="img-thumbnail" alt="${response.content[i].name}"/>
                            </div>
                            <p class="beer-name-abv">${response.content[i].name}, ABV: ${response.content[i].abv}</p>
                            <p class="beer-brewery-country">Brewery: ${response.content[i].brewery.name}, ${response.content[i].country.name}</p>
                            <p class="beer-rating">Average rating:<i class="fa fa-star"></i> ${response.content[i].avgRating}</p>
                        </div>
                    </a>
                </div>
                `);
                    counter += 1;
                })
            }
        }
    })
}

function generateUrl(style, country, tags, page, orderBy) {
    let url = "/api/beers?";

    if (tags !== "") {
        url = url + "tags=" + tags + "&";
    }

    if (style !== "") {
        url = url + "style=" + style + "&";
    }
    if (country !== "") {
        url = url + "country=" + country + "&";
    }
    if (orderBy !== "") {
        url = url + "sort=" + getSortExpression(orderBy) + "&";
    }
    url = url + "page=" + page;
    return url;
}

function getSortExpression(orderBy) {
    let orderMap = {
        'ra': 'avgRating,asc',
        'rd': 'avgRating,desc',
        'aa': 'abv,asc',
        'ad': 'abv,desc',
        'na': 'name,asc',
        'nd': 'name,desc'
    };
    return orderMap[orderBy];
}

