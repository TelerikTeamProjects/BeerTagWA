'use strict';

function deleteUser(id) {
  //  alert(id);

    $.ajax({
        type: "DELETE",
        url: '/api/users/' + id,
        success: function (data) {
            location.reload();
            console.log('success');
        }
    });
}

function changeRole(id) {
    $.ajax({
        type: "PUT",
        url: '/api/users/' + id + '/admin',
        success: function (data) {
            console.log('success');
        }
    });
}