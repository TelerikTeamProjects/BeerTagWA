'use strict';

$(window).bind("load", function () {
    setTimeout(function () {
        getBeerRating();
        getMark(1, '#wish_switch');
        getMark(2, '#drank_switch');
    }, 100);
});

$(document).ready(function () {
    if (username === "anonymousUser") {
        $(".bootstrap-tagsinput").addClass('disabled_tags');
        $(".bootstrap-tagsinput .badge [data-role='remove']").hide();
    }

    $(document).on('change', '#wish_switch', function () {
        if($('#wish_switch').prop("checked") === true){
            addMark(1);
        }else{
            removeMark(1);
        }
    });

    $(document).on('change', '#drank_switch', function () {
        if($('#drank_switch').prop("checked") === true){
            addMark(2);
            alert('Cheers!!!');
        }else{
            removeMark(2);
        }
    });
});

function loadEditPage() {
    $('#edit_beer').hide();
    $('#edit_beer_spin').attr('hidden', false);
    window.location.href = "/beers/" + beerId + "/edit";
}

function getBeerRating() {
    if (username !== "anonymousUser") {
        $.ajax({
            url: "/api/beers/rating?beerId=" + beerId + "&userId=" + window.userIdGlobal,
            success: function (response) {
                if (response > 0) {
                    $('#user-rating').html(`<p>${response}</p>`);
                } else {
                    $('#user-rating').html(`<section class='rating-widget'>
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>`);

                    /* 1. Visualizing things on Hover - See next part for action on click */
                    $('#stars li').on('mouseover', function(){
                        var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                        // Now highlight all the stars that's not after the current hovered star
                        $(this).parent().children('li.star').each(function(e){
                            if (e < onStar) {
                                $(this).addClass('hover');
                            }
                            else {
                                $(this).removeClass('hover');
                            }
                        });

                    }).on('mouseout', function(){
                        $(this).parent().children('li.star').each(function(e){
                            $(this).removeClass('hover');
                        });
                    });


                    /* 2. Action to perform on click */
                    $('#stars li').on('click', function(){
                        var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                        var stars = $(this).parent().children('li.star');

                        for (var i = 0; i < stars.length; i++) {
                            $(stars[i]).removeClass('selected');
                        }

                        for (i = 0; i < onStar; i++) {
                            $(stars[i]).addClass('selected');
                        }
                        rateBeer(onStar);
                    });
                }
            }
        })
    }
}

function rateBeer(rating) {
    let jsonString = JSON.stringify({
        "beerRating": rating,
        "beer": {
                "id": beerId,
                },
        "userId": window.userIdGlobal
    });

    $.ajax({
        type: "POST",
        url: "/api/beers/rate",
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            $('#user-rating').html(`<p>${data.beerRating}</p>`);
            alert('Thank you for rating this beer!');
        }
    });
}

function updateTags() {
    let tags = $('#tags_select').tagsinput('items');
    $('#tags_submit').hide();
    $('#tags_submit_spin').attr('hidden', false);

    let fullTagsListJS = [];
    for (let i = 0; i < fullTagsList.length; i++) {
        fullTagsListJS.push({'id' : fullTagsList[i].id, 'name' : fullTagsList[i].name})
    }

    let beerTagsListJS = [];
    for (let i = 0; i < beerTagsList.length; i++) {
        beerTagsListJS.push({'id' : beerTagsList[i].id, 'name' : beerTagsList[i].name})
    }

    for (let i = 0; i < tags.length; i++) {
        if (fullTagsListJS.filter( t => t.name === tags[i]).length === 0) {
            createTag(tags[i]);
        }
        else if (beerTagsListJS.filter(t => t.name === tags[i]).length === 0) {
            addTagToBeer(beerTagsListJS[fullTagsListJS.indexOf(tags[i])].id, beerId);
        }
    }
    for (let j = 0; j < beerTagsListJS.length; j++) {
        if (tags.filter(t => t === beerTagsListJS[j].name).length === 0) {
            removeTagFromBeer(beerTagsListJS[j].id);
        }
    }
    setTimeout(function () {
        $('#tags_submit_spin').attr('hidden', true);
        $('#tags_submit').show();
    }, 1000);

}

function createTag(tagName) {
    let jsonString = JSON.stringify({
        name: tagName
    });

    $.ajax({
        type: "POST",
        url: "/api/tags/new",
        data: jsonString,
        contentType: 'application/json',
        success: function (data) {
            addTagToBeer(data.id, beerId);
        }
    });
}

function addTagToBeer(tagId) {
    let jsonString = JSON.stringify({
        "beerId": beerId,
        "tagId": tagId,
        "userId": window.userIdGlobal
    });

    $.ajax({
        type: "POST",
        url: "/api/beers/tags/add",
        data: jsonString,
        contentType: 'application/json',
    });
}

function removeTagFromBeer(tagId) {
    let jsonString = JSON.stringify({
        "beerId": beerId,
        "tagId": tagId,
        "userId": window.userIdGlobal
    });

    $.ajax({
        type: "DELETE",
        url: "/api/beers/tags/remove",
        data: jsonString,
        contentType: 'application/json',
    });
}

function addMark(markId) {
    let jsonString = JSON.stringify({
        "beer": {
            "id": beerId
        },
        "userId": window.userIdGlobal,
        "markId": markId
    });

    $.ajax({
        type: "POST",
        url: "/api/user/marks/new",
        data: jsonString,
        contentType: 'application/json',
    });
}

function removeMark(markId) {
    let jsonString = JSON.stringify({
        "beer": {
            "id": beerId
        },
        "userId": window.userIdGlobal,
        "markId": markId
    });

    $.ajax({
        type: "DELETE",
        url: "/api/user/marks/remove",
        data: jsonString,
        contentType: 'application/json',
    });
}

function getMark(markId, markSelector) {
    $.ajax({
        url: "/api/user/marks/get?beerId=" + beerId + "&userId=" + window.userIdGlobal + "&markId=" + markId,
        success: function (response) {
            if (response.length > 0) {
                $(markSelector).prop("checked", true);
            }
        }
    });
}

