generateUserProfilePage();

function generateUserProfilePage() {
    if (document.getElementById("user-profile-page") !== null) {
        let url = "/users/userprofile/";
        let id;
        let username = document.getElementById("user-profile-page").innerText;
        $.ajax({
            url: "/api/users?username=" + username,
            success: function (response) {
                var result = response[0];
                /*console.log(result);*/
                id = result.id;
                window.userIdGlobal = result.id;
                /*console.log(id);*/
                url = url + id;
                /*console.log(url);*/
                document.getElementById("user-profile-page").setAttribute("href", url);
                return id;
            }
        })
    }
}