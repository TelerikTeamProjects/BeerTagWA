'use strict';
loadRatedBeers();
loadWishlist();

function loadRatedBeers() {
    // console.log('test');
    $.ajax({
        url: "/api/users/" + userId + "/topbeers",
        success: function (response) {
            if (response.length === 0) {
                $('#ratedBeers').append(`<img src="/images/vote_now.png" width="33%" class="img-thumbnail no-ratedbeers" alt="RatedBeers">`);

            } else {
                // console.log(response);
                $.each(response, function (i, v) {
                    $('#ratedBeers')
                        .append(`
                    <div class="col-md-4 beer-col text-left">
                    <a class="beer-link" href="/beers/${response[i].beer.id}">
                        <div class="image holder text-center">
                            <img src="data:image/png;base64, ${response[i].beer.image.image}" class="img-thumbnail" alt=">${response[i].beer.name}"/>
                            </div>
                            <p class="beer-name-abv mt-3">${response[i].beer.name}, ABV: ${response[i].beer.abv}</p>
                            <p class="beer-brewery-country">Brewery: ${response[i].beer.brewery.name}, ${response[i].beer.country.name}</p>
                            <p class="beer-rating">Average rating:<i class="fa fa-star"></i> ${response[i].beer.avgRating}</p
                    </a>
                    </div>
                    `)
                })
            }
        }
    })
}

function loadWishlist() {
    // console.log('test');
    $.ajax({
        url: "/api/users/" + userId + "/wishlist",
        success: function (response) {
            if (response.length === 0) {
                $('#wishlist').append(`<img src="/images/dream_beer.png" width="50%" class="img-thumbnail no-markedbeers" alt="Wishlist">`);

            } else {
                // console.log(response);
                $.each(response, function (i, v) {
                    $('#wishlist')
                        .append(`
                     <div class="col-md-4 beer-col text-left">
                    <a class="beer-link" href="/beers/${response[i].beer.id}">
                        <div class="image holder text-center">
                            <img src="data:image/png;base64, ${response[i].beer.image.image}" class="img-thumbnail" alt=">${response[i].beer.name}"/>
                            </div>
                            <p class="beer-name-abv mt-3">${response[i].beer.name}, ABV: ${response[i].beer.abv}</p>
                            <p class="beer-brewery-country">Brewery: ${response[i].beer.brewery.name}, ${response[i].beer.country.name}</p>
                            <p class="beer-rating">Average rating:<i class="fa fa-star"></i> ${response[i].beer.avgRating}</p
                    </a>
                    </div>
                    `)
                })
            }
        }
    })
}

$(document).ready(function () {
    var readURL = function (input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".file-input").on('change', function () {
        readURL(this);
    });
});

$('#singleUploadForm').submit(function (event) {
    var formElement = this;
    var formData = new FormData(formElement);

    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/api/userimages/upload/" + userId,
        data: formData,
        processData: false,
        contentType: false,
        success: function (response) {
            singleFileUploadError.style.display = "none";
            singleFileUploadSuccess.innerHTML = "<p>File Uploaded Successfully.</p>"; /*+
                "<p>DownloadUrl : <a href='" + response.fileDownloadUri + "' target='_blank'>" + response.fileDownloadUri + "</a></p>";*/
            singleFileUploadSuccess.style.display = "block";
        },
        error: function (error) {
            singleFileUploadSuccess.style.display = "none";
            singleFileUploadError.innerHTML = "<p>Some Error Occurred</p>"
            singleFileUploadError.style.display = "block";
        }
    });

    event.preventDefault();
});

$('#userUpdateForm').onclick(function (event) {
    var form = $(this);
    var firstName = form.find('input[name="firstName"]').val();
    var middleName = form.find('input[name="middleName"]').val();
    var lastName = form.find('input[name="lastName"]').val();

    var jsonString = JSON.stringify({
        firstName: firstName,
        middleName: middleName,
        lastName: lastName
    });
    console.log(jsonString);

    $.ajax({
        type: 'PUT',
        url: "/api/users/" + userId,
        contentType: 'application/json',
        data: jsonString, // access in body
    }).done(function (response) {
        console.log('SUCCESS');
        console.log(response)
    }).fail(function (msg) {
        console.log('FAIL');
    }).always(function (msg) {
        console.log('ALWAYS');
    });

    event.preventDefault();
});