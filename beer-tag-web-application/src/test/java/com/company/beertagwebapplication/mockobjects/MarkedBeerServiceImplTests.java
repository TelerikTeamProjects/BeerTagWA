package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.repostories.MarkedBeerRepository;
import com.company.beertagwebapplication.services.MarkedBeerServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MarkedBeerServiceImplTests {
    @Mock
    private MarkedBeerRepository mockMarkedBeerRepository;

    @InjectMocks
    private MarkedBeerServiceImpl markedBeerService;

    @Test
    public void getMarkedBeers_Should_ReturnMarkedBeerList_When_MarkedBeerExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);
        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(Arrays.asList(
                        new MarkedBeer(1, beer, 1, 1)
                ));

        // Act
        List<MarkedBeer> result = markedBeerService.get(markedBeer);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void add_Should_CallRepositoryAdd_When_BeerIdNotMarked() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(new ArrayList<>());

        // Act
        markedBeerService.add(markedBeer);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.times(1)).add(markedBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_Should_ThrowException_When_MarkedBeerExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(Arrays.asList(
                        new MarkedBeer(1, beer, 1, 1)
                ));

        // Act
        markedBeerService.add(markedBeer);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.never()).add(markedBeer);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_MarkedBeerMarksAreDifferent() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(Arrays.asList(
                        new MarkedBeer(1, beer, 1, 2)
                ));

        Mockito.when(mockMarkedBeerRepository.update(markedBeer))
                .thenReturn(1);

        // Act
        int result = markedBeerService.update(markedBeer);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.times(1)).update(markedBeer);
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_MarkedBeerMarkExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(new ArrayList<>());

        // Act
        markedBeerService.update(markedBeer);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.never()).update(markedBeer);
    }

    @Test
    public void remove_Should_ReturnAffectedRows_When_MarkedBeerMarkIsDeleted() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(Arrays.asList(
                        new MarkedBeer(1, beer, 1, 1)
                ));

        Mockito.when(mockMarkedBeerRepository.remove(markedBeer))
                .thenReturn(1);

        // Act
        int result = markedBeerService.remove(markedBeer);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.times(1)).remove(markedBeer);
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void remove_Should_ThrowException_When_MarkedBeerMarkNotExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockMarkedBeerRepository.get(markedBeer))
                .thenReturn(new ArrayList<>());

        // Act
        markedBeerService.remove(markedBeer);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.never()).remove(markedBeer);
    }
}
