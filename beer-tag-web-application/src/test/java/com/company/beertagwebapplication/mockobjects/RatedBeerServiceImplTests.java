package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.repostories.RatedBeerRepository;
import com.company.beertagwebapplication.services.RatedBeerServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RatedBeerServiceImplTests {
    @Mock
    private RatedBeerRepository mockRatedBeerRepository;

    @InjectMocks
    private RatedBeerServiceImpl ratedBeerService;

    @Test
    public void getRatedBeers_Should_ReturnRatedBeerList_When_RatedBeerExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        RatedBeer ratedBeer = new RatedBeer(1, 4, beer, 1);
        Mockito.when(mockRatedBeerRepository.get(ratedBeer))
                .thenReturn(Arrays.asList(
                        new RatedBeer(1, 4, beer, 1)
                ));

        // Act
        List<RatedBeer> result = ratedBeerService.get(ratedBeer);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void add_Should_CallRepositoryAdd_When_BeerIdNotRated() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        RatedBeer ratedBeer = new RatedBeer(1, 4, beer, 1);

        Mockito.when(mockRatedBeerRepository.get(ratedBeer))
                .thenReturn(new ArrayList<>());

        // Act
        ratedBeerService.add(ratedBeer);

        // Assert
        Mockito.verify(mockRatedBeerRepository, Mockito.times(1)).add(ratedBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_MarkedBeerExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        RatedBeer ratedBeer = new RatedBeer(1, 4, beer, 1);

        Mockito.when(mockRatedBeerRepository.get(ratedBeer))
                .thenReturn(Arrays.asList(
                        new RatedBeer(1, 4, beer, 1)
                ));

        // Act
        ratedBeerService.add(ratedBeer);

        // Assert
        Mockito.verify(mockRatedBeerRepository, Mockito.never()).add(ratedBeer);
    }
}
