package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.repostories.BeerImageRepository;
import com.company.beertagwebapplication.repostories.BeerRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.BeerImageServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BeerImageServiceImplTests {
    @Mock
    private BeerImageRepository mockBeerImageRepository;

    @Mock
    private BeerRepository mockBeerRepository;

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private BeerImageServiceImpl beerImageService;

    @Test
    public void storeFile_Should_CallRepositoryCreate_When_BeerAndFileExist() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        BeerImage newImage = new BeerImage(1, "Image", "Type", null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(new Beer(1, "Beer One", brewery, country, 8.5,
                        "Test Beer", style, 7.5, user, true, user, null
                ));

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                        "Last Name", null, true
                ));

        Mockito.when(mockBeerImageRepository.save(newImage))
                .thenReturn(newImage);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        // Act
        beerImageService.storeFile(newImage, 1, 1);

        // Assert
        Mockito.verify(mockBeerImageRepository, Mockito.times(1)).save(newImage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storeFile_Should_ThrowException_When_BeerNotExists() {
        // Arrange
        BeerImage newImage = new BeerImage(1, "Image", "Type", null);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                        "Last Name", null, true
                ));
        // Act
        beerImageService.storeFile(newImage, 1, 1);

        // Assert
        Mockito.verify(mockBeerImageRepository, Mockito.never()).save(newImage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storeFile_Should_ThrowException_When_UserNotExists() {
        // Arrange
        BeerImage newImage = new BeerImage(1, "Image", "Type", null);
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(new Beer(1, "Beer One", brewery, country, 8.5,
                        "Test Beer", style, 7.5, user, true, user, null
                ));
        // Act
        beerImageService.storeFile(newImage, 1, 1);

        // Assert
        Mockito.verify(mockBeerImageRepository, Mockito.never()).save(newImage);
    }

    @Test
    public void getFile_Should_ReturnBeerImage_When_IdExists() {
        // Arrange
        Mockito.when(mockBeerImageRepository.findById(1))
                .thenReturn(java.util.Optional.of(new BeerImage(1, "Image", "Type", null)));

        // Act
        BeerImage image = beerImageService.getFile(1);

        // Assert
        Assert.assertEquals(1, image.getBeerId());
        Assert.assertEquals("Image", image.getFileName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFile_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        beerImageService.getFile(1);

        // Assert
        Mockito.verify(mockBeerImageRepository, Mockito.never()).findById(1);
    }
}
