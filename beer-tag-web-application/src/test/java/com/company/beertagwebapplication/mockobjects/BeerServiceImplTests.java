package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.repostories.BeerRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.BeerServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {
    @Mock
    private BeerRepository mockBeerRepository;

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private BeerServiceImpl beerService;

    @Test
    public void getBeers_Should_ReturnBeerList_When_FilterNotExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        List<Beer> beers = new ArrayList<>();
        beers.add(beer);
        Page<Beer> pagedResponse = new PageImpl((Arrays.asList(beers)));
        Mockito.when(mockBeerRepository
                .get(any(), any(), any(), any()))
                .thenReturn(pagedResponse);
        // Act
        Page<Beer> result = beerService.get(any(), any(), any(), any());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }

    @Test
    public void getBeers_Should_ReturnBeerList_When_FilterExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        List<Beer> beers = new ArrayList<>();
        beers.add(beer);
        Page<Beer> pagedResponse = new PageImpl((Arrays.asList(beers)));
        Mockito.when(mockBeerRepository
                .get(null, "Pale Ale", null, Pageable.unpaged()))
                .thenReturn(pagedResponse);
        // Act
        Page<Beer> result = beerService.get(null, "Pale Ale", null, Pageable.unpaged());

        // Assert
        Assert.assertEquals(1, result.getTotalPages());
        Assert.assertEquals(1, result.getTotalElements());
        Assert.assertEquals(1, result.getContent().size());
    }

    @Test
    public void getByIdBeer_Should_ReturnBeer_When_IdExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(beer);

        // Act
        Beer result = beerService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Beer One", result.getName());
    }


    @Test(expected = IllegalArgumentException.class)
    public void getByIdBeer_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        beerService.getById(1);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).getById(1);
    }

    @Test
    public void getByName_Should_ReturnBeer_When_NameExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getByName("Beer One"))
                .thenReturn(Arrays.asList(beer));

        // Act
        Beer result = beerService.getByName("Beer One");

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Beer One", result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByName_Should_ThrowException_When_NameNotExist() {
        // Arrange

        // Act
        Beer result = beerService.getByName("Beer One");

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).getByName("Beer One");
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_NameIsUnique() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer newBeer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        // Act
        beerService.create(newBeer);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.times(1)).create(newBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsNotUnique() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer newBeer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getByName(newBeer.getName()))
                .thenReturn(Arrays.asList(
                       newBeer
                ));

        // Act
        beerService.create(newBeer);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).create(newBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_InvalidParameterIsPassed(){
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer newBeer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.doThrow(IllegalArgumentException.class).when(mockBeerRepository).create(isA(Beer.class));

        // Act
        beerService.create(newBeer);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).create(newBeer);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_BeerNamesAreDifferent() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        Beer newBeer = new Beer(1, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(beer);

        Mockito.when(mockBeerRepository.getByName(newBeer.getName()))
                .thenReturn(new ArrayList<>());

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        Mockito.when(mockBeerRepository.update(1, newBeer, 1))
                .thenReturn(1);

        // Act
        int result = beerService.update(1, newBeer, 1);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_BeerNotExist() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        Beer newBeer = new Beer(1, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        // Act
        beerService.update(1, newBeer, 1);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).update(1, newBeer, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_NewBeerNameExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        Beer newBeer = new Beer(1, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(beer);

        Mockito.when(mockBeerRepository.getByName(newBeer.getName()))
                .thenReturn(Arrays.asList(newBeer));

        // Act
        beerService.update(1, newBeer, 1);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).update(1, newBeer, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_UserNotExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        Beer newBeer = new Beer(1, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        Beer existingBeer = new Beer(2, "Beer Update", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(beer);

        Mockito.when(mockBeerRepository.getByName(newBeer.getName()))
                .thenReturn(Arrays.asList(existingBeer));

        // Act
        beerService.update(1, newBeer, 1);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).update(1, newBeer, 1);
    }

    @Test
    public void delete_Should_ReturnAffectedRows_When_BeerIsDeleted() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(beer);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        Mockito.when(mockBeerRepository.delete(1,1))
                .thenReturn(1);

        // Act
        int result = beerService.delete(1, 1);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_BeerNotExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        // Act
        beerService.delete(1, 1);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).delete(1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_UserNotExists() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);

        Mockito.when(mockBeerRepository.getById(1))
                .thenReturn(beer);

        // Act
        beerService.delete(1, 1);

        // Assert
        Mockito.verify(mockBeerRepository, Mockito.never()).delete(1, 1);
    }
}
