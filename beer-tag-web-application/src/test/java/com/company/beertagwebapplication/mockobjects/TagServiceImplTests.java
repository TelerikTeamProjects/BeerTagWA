package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.Tag;
import com.company.beertagwebapplication.repostories.TagRepository;
import com.company.beertagwebapplication.services.TagServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {
    @Mock
    private TagRepository mockTagRepository;

    @InjectMocks
    private TagServiceImpl tagService;

    @Test
    public void getTags_Should_ReturnTagList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockTagRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Tag(1, "Tag One"),
                        new Tag(2, "Tag Two"),
                        new Tag(3, "Tag Three")
                ));

        // Act
        List<Tag> result = tagService.get(null);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getTags_Should_ReturnTag_When_FilterExists() {
        // Arrange
        Mockito.when(mockTagRepository.getByName("Tag Two"))
                .thenReturn(Arrays.asList(new Tag(2, "Tag Two")));

        // Act
        List<Tag> result = tagService.get("Tag Two");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getTags_Should_ReturnEmptyTagList_When_FilterNotMatch() {
        // Arrange
        Mockito.when(tagService.get("Test Tag"))
                .thenReturn(new ArrayList<>());

        // Act
        List<Tag> result = tagService.get("Test Tag");

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getByIdTag_Should_ReturnTag_When_IdExists() {
        // Arrange
        Mockito.when(mockTagRepository.getById(1))
                .thenReturn(new Tag(1, "Tag One"));

        // Act
        Tag result = tagService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Tag One", result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdTag_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        tagService.getById(1);

        // Assert
        Mockito.verify(mockTagRepository, Mockito.never()).getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_NameIsUnique() {
        // Arrange
        Tag newTag = new Tag(1, "Tag 1");

        // Act
        tagService.create(newTag);

        // Assert
        Mockito.verify(mockTagRepository, Mockito.times(1)).create(newTag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsNotUnique() {
        // Arrange
        Tag newTag = new Tag(1, "Tag One");
        Mockito.when(mockTagRepository.getByName(newTag.getName()))
                .thenReturn(Arrays.asList(
                        new Tag(1, "Tag One"),
                        new Tag(2, "Tag Two"),
                        new Tag(3, "Tag Three")
                ));

        // Act
        tagService.create(newTag);

        // Assert
        Mockito.verify(mockTagRepository, Mockito.never()).create(newTag);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_TagNamesAreDifferent() {
        // Arrange
        Tag newTag = new Tag(1, "New Tag");

        Mockito.when(mockTagRepository.getById(1))
                .thenReturn(new Tag(1, "Tag One"));

        Mockito.when(mockTagRepository.update(1, newTag))
                .thenReturn(1);

        // Act
        int result = tagService.update(1, newTag);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_TagNotExists() {
        // Arrange
        Tag newTag = new Tag(1, "New Tag");

        // Act
        tagService.update(1, newTag);

        // Assert
        Mockito.verify(mockTagRepository, Mockito.never()).update(1, newTag);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_TagNameExists() {
        // Arrange
        Tag newTag = new Tag(1, "New Tag");


        Mockito.when(mockTagRepository.getById(1))
                .thenReturn(new Tag(1, "Tag"));

        Mockito.when(mockTagRepository.getByName(newTag.getName()))
                .thenReturn((Arrays.asList(
                        new Tag(2, "New Tag"))));

        // Act
        tagService.update(1, newTag);

        // Assert
        Mockito.verify(mockTagRepository, Mockito.never()).update(1, newTag);
    }
}