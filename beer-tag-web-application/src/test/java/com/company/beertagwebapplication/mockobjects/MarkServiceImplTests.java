package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.Mark;
import com.company.beertagwebapplication.repostories.MarkRepository;
import com.company.beertagwebapplication.services.MarkServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class MarkServiceImplTests {
    @Mock
    private MarkRepository mockMarkRepository;

    @InjectMocks
    private MarkServiceImpl markService;

    @Test
    public void getMarks_Should_ReturnMarkList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockMarkRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Mark(1, "Mark One"),
                        new Mark(2, "Mark Two"),
                        new Mark(3, "Mark Three")
                ));

        // Act
        List<Mark> result = markService.get(null);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getMarks_Should_ReturnMark_When_FilterExists() {
        // Arrange
        Mockito.when(mockMarkRepository.getByName("Mark Two"))
                .thenReturn(Arrays.asList(new Mark(2, "Mark Two")));

        // Act
        List<Mark> result = markService.get("Mark Two");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getMarks_Should_ReturnEmptyMarkList_When_FilterNotMatch() {
        // Arrange
        Mockito.when(markService.get("Test Mark"))
                .thenReturn(new ArrayList<>());

        // Act
        List<Mark> result = markService.get("Test Mark");

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getByIdMark_Should_ReturnMark_When_IdExists() {
        // Arrange
        Mockito.when(mockMarkRepository.getById(1))
                .thenReturn(new Mark(1, "Mark One"));

        // Act
        Mark result = markService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Mark One", result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdMark_Should_ThrowException_When_IdNotExists() {
        // Arrange
        Mockito.when(markService.getById(1))
                .thenReturn(null);

        // Act
        markService.getById(1);

        // Assert
        Mockito.verify(mockMarkRepository, Mockito.never()).getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_NameIsUnique() {
        // Arrange
        Mark newMark = new Mark(1, "Mark 1");

        // Act
        markService.create(newMark);

        // Assert
        Mockito.verify(mockMarkRepository, Mockito.times(1)).create(newMark);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsNotUnique() {
        // Arrange
        Mark newMark = new Mark(1, "Mark One");
        Mockito.when(mockMarkRepository.getByName(newMark.getName()))
                .thenReturn(Arrays.asList(
                        new Mark(1, "Mark One"),
                        new Mark(2, "Mark Two"),
                        new Mark(3, "Mark Three")
                ));

        // Act
        markService.create(newMark);

        // Assert
        Mockito.verify(mockMarkRepository, Mockito.never()).create(newMark);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_MarkNamesAreDifferent() {
        // Arrange
        Mark newMark = new Mark(1, "New Mark");

        Mockito.when(mockMarkRepository.getById(1))
                .thenReturn(new Mark(1, "Mark One"));

        Mockito.when(mockMarkRepository.update(1, newMark))
                .thenReturn(1);

        // Act
        int result = markService.update(1, newMark);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_MarkNotExists() {
        // Arrange
        Mark newMark = new Mark(1, "New Mark");

        // Act
        markService.update(1, newMark);

        // Assert
        Mockito.verify(mockMarkRepository, Mockito.never()).update(1, newMark);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_MarkNameExists() {
        // Arrange
        Mark newMark = new Mark(1, "New Mark");


        Mockito.when(mockMarkRepository.getById(1))
                .thenReturn(new Mark(1, "Mark"));

        Mockito.when(mockMarkRepository.getByName(newMark.getName()))
                .thenReturn((Arrays.asList(
                        new Mark(2, "New Mark"))));

        // Act
        markService.update(1, newMark);

        // Assert
        Mockito.verify(mockMarkRepository, Mockito.never()).update(1, newMark);
    }
}