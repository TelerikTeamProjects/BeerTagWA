package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.Brewery;
import com.company.beertagwebapplication.repostories.BreweryRepository;
import com.company.beertagwebapplication.services.BreweryServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {
    @Mock
    private BreweryRepository mockBreweryRepository;

    @InjectMocks
    private BreweryServiceImpl breweryService;

    @Test
    public void getBreweries_Should_ReturnBreweryList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockBreweryRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Brewery(1, "Brewery One"),
                        new Brewery(2, "Brewery Two"),
                        new Brewery(3, "Brewery Three")
                ));

        // Act
        List<Brewery> result = breweryService.get(null);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getBreweries_Should_ReturnBrewery_When_FilterExists() {
        // Arrange
        Mockito.when(mockBreweryRepository.getByName("Brewery Two"))
                .thenReturn(Arrays.asList(new Brewery(2, "Brewery Two")));

        // Act
        List<Brewery> result = breweryService.get("Brewery Two");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getBreweries_Should_ReturnEmptyBreweryList_When_FilterNotMatch() {
        // Arrange
        Mockito.when(breweryService.get("Test Brewery"))
                .thenReturn(new ArrayList<>());

        // Act
        List<Brewery> result = breweryService.get("Test Brewery");

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getByIdBrewery_Should_ReturnBrewery_When_IdExists() {
        // Arrange
        Mockito.when(mockBreweryRepository.getById(1))
                .thenReturn(new Brewery(1, "Brewery One"));

        // Act
        Brewery result = breweryService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Brewery One", result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdBrewery_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        breweryService.getById(1);

        // Assert
        Mockito.verify(mockBreweryRepository, Mockito.never()).getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_NameIsUnique() {
        // Arrange
        Brewery newBrewery = new Brewery(1, "Brewery 1");

        // Act
        breweryService.create(newBrewery);

        // Assert
        Mockito.verify(mockBreweryRepository, Mockito.times(1)).create(newBrewery);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsNotUnique() {
        // Arrange
        Brewery newBrewery = new Brewery(1, "Brewery One");
        Mockito.when(mockBreweryRepository.getByName(newBrewery.getName()))
                .thenReturn(Arrays.asList(
                        new Brewery(1, "Brewery One"),
                        new Brewery(2, "Brewery Two"),
                        new Brewery(3, "Brewery Three")
                ));

        // Act
        breweryService.create(newBrewery);

        // Assert
        Mockito.verify(mockBreweryRepository, Mockito.never()).create(newBrewery);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_BreweryNamesAreDifferent() {
        // Arrange
        Brewery newBrewery = new Brewery(1, "New Brewery");

        Mockito.when(mockBreweryRepository.getById(1))
                .thenReturn(new Brewery(1, "Brewery One"));

        Mockito.when(mockBreweryRepository.update(1, newBrewery))
                .thenReturn(1);

        // Act
        int result = breweryService.update(1, newBrewery);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_BreweryNotExists() {
        // Arrange
        Brewery newBrewery = new Brewery(1, "New Brewery");

        // Act
        breweryService.update(1, newBrewery);

        // Assert
        Mockito.verify(mockBreweryRepository, Mockito.never()).update(1, newBrewery);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_NewBreweryNameExists() {
        // Arrange
        Brewery newBrewery = new Brewery(1, "New Brewery");

        Mockito.when(mockBreweryRepository.getById(1))
                .thenReturn(new Brewery(1, "Brewery"));

        Mockito.when(mockBreweryRepository.getByName("New Brewery"))
                .thenReturn(Arrays.asList(
                        new Brewery(2, "New Brewery")));

        // Act
        breweryService.update(1, newBrewery);

        // Assert
        Mockito.verify(mockBreweryRepository, Mockito.never()).update(1, newBrewery);
    }
}