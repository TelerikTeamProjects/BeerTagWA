package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.Style;
import com.company.beertagwebapplication.repostories.StyleRepository;
import com.company.beertagwebapplication.services.StyleServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {
    @Mock
    private StyleRepository mockStyleRepository;

    @InjectMocks
    private StyleServiceImpl styleService;

    @Test
    public void getStyles_Should_ReturnStyleList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockStyleRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Style(1, "Style One"),
                        new Style(2, "Style Two"),
                        new Style(3, "Style Three")
                ));

        // Act
        List<Style> result = styleService.get(null);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getStyles_Should_ReturnStyle_When_FilterExists() {
        // Arrange
        Mockito.when(mockStyleRepository.getByName("Style Two"))
                .thenReturn(Arrays.asList(new Style(2, "Style Two")));

        // Act
        List<Style> result = styleService.get("Style Two");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getStyles_Should_ReturnEmptyStyleList_When_FilterNotMatch() {
        // Arrange
        Mockito.when(styleService.get("Test Style"))
                .thenReturn(new ArrayList<>());

        // Act
        List<Style> result = styleService.get("Test Style");

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getByIdStyle_Should_ReturnStyle_When_IdExists() {
        // Arrange
        Mockito.when(mockStyleRepository.getById(1))
                .thenReturn(new Style(1, "Style One"));

        // Act
        Style result = styleService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Style One", result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdStyle_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        styleService.getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_NameIsUnique() {
        // Arrange
        Style newStyle = new Style(1, "Style 1");

        // Act
        styleService.create(newStyle);

        // Assert
        Mockito.verify(mockStyleRepository, Mockito.times(1)).create(newStyle);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsNotUnique() {
        // Arrange
        Style newStyle = new Style(1, "Style One");
        Mockito.when(mockStyleRepository.getByName(newStyle.getName()))
                .thenReturn(Arrays.asList(
                        new Style(1, "Style One"),
                        new Style(2, "Style Two"),
                        new Style(3, "Style Three")
                ));

        // Act
        styleService.create(newStyle);

        // Assert
        Mockito.verify(mockStyleRepository, Mockito.never()).create(newStyle);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_StyleNamesAreDifferent() {
        // Arrange
        Style newStyle = new Style(1, "New Style");

        Mockito.when(mockStyleRepository.getById(1))
                .thenReturn(new Style(1, "Style One"));

        Mockito.when(mockStyleRepository.update(1, newStyle))
                .thenReturn(1);

        // Act
        int result = styleService.update(1, newStyle);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_StyleNotExists() {
        // Arrange
        Style newStyle = new Style(1, "New Style");

        // Act
        styleService.update(1, newStyle);

        // Assert
        Mockito.verify(mockStyleRepository, Mockito.never()).update(1, newStyle);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_StyleNameExists() {
        // Arrange
        Style newStyle = new Style(1, "New Style");


        Mockito.when(mockStyleRepository.getById(1))
                .thenReturn(new Style(1, "Style"));

        Mockito.when(mockStyleRepository.getByName(newStyle.getName()))
                .thenReturn((Arrays.asList(
                        new Style(2, "New Style"))));

        // Act
        styleService.update(1, newStyle);

        // Assert
        Mockito.verify(mockStyleRepository, Mockito.never()).update(1, newStyle);
    }
}