package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.UserDetails;
import com.company.beertagwebapplication.models.UserImage;
import com.company.beertagwebapplication.repostories.UserImageRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.UserImageServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserImageServiceImplTests {
    @Mock
    private UserImageRepository mockUserImageRepository;

    @Mock
    private UserRepository mockUserRepository;

    @InjectMocks
    private UserImageServiceImpl userImageService;

    @Test
    public void storeFile_Should_CallRepositoryCreate_When_UserAndFileExist() {
        // Arrange
        UserImage newImage = new UserImage(1, "Image", "Type", null);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                        "Last Name", null, true));

        Mockito.when(mockUserImageRepository.save(newImage))
                .thenReturn(newImage);

        // Act
        userImageService.storeFile(newImage, 1);

        // Assert
        Mockito.verify(mockUserImageRepository, Mockito.times(1)).save(newImage);
    }

    @Test(expected = IllegalArgumentException.class)
    public void storeFile_Should_ThrowException_When_UserNotExists() {
        // Arrange
        UserImage newImage = new UserImage(1, "Image", "Type", null);

        // Act
        userImageService.storeFile(newImage, 1);

        // Assert
        Mockito.verify(mockUserImageRepository, Mockito.never()).save(newImage);
    }

    @Test
    public void getFile_Should_ReturnUserImage_When_IdExists() {
        // Arrange
        Mockito.when(mockUserImageRepository.findById(1))
                .thenReturn(java.util.Optional.of(new UserImage(1, "Image", "Type", null)));

        // Act
        UserImage image = userImageService.getFile(1);

        // Assert
        Assert.assertEquals(1, image.getUserId());
        Assert.assertEquals("Image", image.getFileName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFile_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        userImageService.getFile(1);

        // Assert
        Mockito.verify(mockUserImageRepository, Mockito.never()).findById(1);
    }
}
