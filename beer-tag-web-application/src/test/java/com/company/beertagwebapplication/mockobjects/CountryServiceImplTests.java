package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.Country;
import com.company.beertagwebapplication.repostories.CountryRepository;
import com.company.beertagwebapplication.services.CountryServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {
    @Mock
    private CountryRepository mockCountryRepository;

    @InjectMocks
    private CountryServiceImpl countryService;

    @Test
    public void getCountries_Should_ReturnCountryList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockCountryRepository.getAll())
                .thenReturn(Arrays.asList(
                        new Country(1, "Country One"),
                        new Country(2, "Country Two"),
                        new Country(3, "Country Three")
                ));

        // Act
        List<Country> result = countryService.get(null);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getCountries_Should_ReturnCountry_When_FilterExists() {
        // Arrange
        Mockito.when(mockCountryRepository.getByName("Country Two"))
                .thenReturn(Arrays.asList(new Country(2, "Country Two")));

        // Act
        List<Country> result = countryService.get("Country Two");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getCountries_Should_ReturnEmptyCountryList_When_FilterNotMatch() {
        // Arrange
        Mockito.when(countryService.get("Test Country"))
                .thenReturn(new ArrayList<>());

        // Act
        List<Country> result = countryService.get("Test Country");

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getByIdCountry_Should_ReturnCountry_When_IdExists() {
        // Arrange
        Mockito.when(mockCountryRepository.getById(1))
                .thenReturn(new Country(1, "Country One"));

        // Act
        Country result = countryService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("Country One", result.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdCountry_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        countryService.getById(1);

        // Assert
        Mockito.verify(mockCountryRepository, Mockito.never()).getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_NameIsUnique() {
        // Arrange
        Country newCountry = new Country(1, "Country 1");

        // Act
        countryService.create(newCountry);

        // Assert
        Mockito.verify(mockCountryRepository, Mockito.times(1)).create(newCountry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_NameIsNotUnique() {
        // Arrange
        Country newCountry = new Country(1, "Country One");
        Mockito.when(mockCountryRepository.getByName(newCountry.getName()))
                .thenReturn(Arrays.asList(
                        new Country(1, "Country One"),
                        new Country(2, "Country Two"),
                        new Country(3, "Country Three")
                ));

        // Act
        countryService.create(newCountry);

        // Assert
        Mockito.verify(mockCountryRepository, Mockito.never()).create(newCountry);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_CountryNamesAreDifferent() {
        // Arrange
        Country newCountry = new Country(1, "New Country");

        Mockito.when(mockCountryRepository.getById(1))
                .thenReturn(new Country(1, "Country One"));

        Mockito.when(mockCountryRepository.update(1, newCountry))
                .thenReturn(1);

        // Act
        int result = countryService.update(1, newCountry);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_CountryNotExists() {
        // Arrange
        Country newCountry = new Country(1, "New Country");

        // Act
        countryService.update(1, newCountry);

        // Assert
        Mockito.verify(mockCountryRepository, Mockito.never()).update(1, newCountry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_CountryNameExists() {
        // Arrange
        Country newCountry = new Country(1, "New Country");


        Mockito.when(mockCountryRepository.getById(1))
                .thenReturn(new Country(1, "Country"));

        Mockito.when(mockCountryRepository.getByName(newCountry.getName()))
                .thenReturn((Arrays.asList(
                        new Country(2, "New Country"))));

        // Act
        countryService.update(1, newCountry);

        // Assert
        Mockito.verify(mockCountryRepository, Mockito.never()).update(1, newCountry);
    }
}
