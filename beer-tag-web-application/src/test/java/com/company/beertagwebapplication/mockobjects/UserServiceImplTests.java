package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.repostories.MarkedBeerRepository;
import com.company.beertagwebapplication.repostories.RatedBeerRepository;
import com.company.beertagwebapplication.repostories.UserRepository;
import com.company.beertagwebapplication.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private RatedBeerRepository mockRatedBeerRepository;

    @Mock
    private MarkedBeerRepository mockMarkedBeerRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getUsers_Should_ReturnUsersList_When_FilterNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.getAll())
                .thenReturn(Arrays.asList(
                        new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                                "Last Name", null, true),
                        new UserDetails(1, "test2@test.test", "Second Name", "Middle Name",
                                "Last Name", null, true),
                        new UserDetails(1, "test3@test.test", "Third Name", "Middle Name",
                                "Last Name", null, true)
                ));

        // Act
        List<UserDetails> result = userService.get(null);

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getUsers_Should_ReturnUser_When_FilterExists() {
        // Arrange
        Mockito.when(mockUserRepository.getByName("test@test.test"))
                .thenReturn(Arrays.asList(new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                        "Last Name", null, true)));

        // Act
        List<UserDetails> result = userService.get("test@test.test");

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getUsers_Should_ReturnEmptyBreweryList_When_FilterNotMatch() {
        // Arrange
        Mockito.when(userService.get("Test User"))
                .thenReturn(new ArrayList<>());

        // Act
        List<UserDetails> result = userService.get("Test User");

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test
    public void getByIdUser_Should_ReturnUser_When_IdExists() {
        // Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                        "Last Name", null, true));

        // Act
        UserDetails result = userService.getById(1);

        // Assert
        Assert.assertEquals(1, result.getId());
        Assert.assertEquals("test1@test.test", result.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getByIdUser_Should_ThrowException_When_IdNotExists() {
        // Arrange

        // Act
        userService.getById(1);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.never()).getById(1);
    }

    @Test
    public void create_Should_CallRepositoryCreate_When_UsernameIsUnique() {
        // Arrange
        UserDetails newUser = new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        // Act
        userService.create(newUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1)).create(newUser);
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_Should_ThrowException_When_UsernameIsNotUnique() {
        // Arrange
        UserDetails newUser = new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Mockito.when(mockUserRepository.getByName(newUser.getUsername()))
                .thenReturn(Arrays.asList(
                        new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                                "Last Name", null, true),
                        new UserDetails(1, "test2@test.test", "Second Name", "Middle Name",
                                "Last Name", null, true),
                        new UserDetails(1, "test3@test.test", "Third Name", "Middle Name",
                                "Last Name", null, true)
                ));

        // Act
        userService.create(newUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.never()).create(newUser);
    }

    @Test
    public void update_Should_ReturnAffectedRows_When_UsernamesAreDifferent() {
        // Arrange
        UserDetails newUser = new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                        "Last Name", null, true));

        Mockito.when(mockUserRepository.update(1, newUser))
                .thenReturn(1);

        // Act
        int result = userService.update(1, newUser);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_Should_ThrowException_When_UsernameIdNotExists() {
        // Arrange
        UserDetails newUser = new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        // Act
        userService.update(1, newUser);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.never()).update(1, newUser);
    }

    @Test
    public void delete_Should_ReturnAffectedRows_When_IdExists() {
        // Arrange
        UserDetails newUser = new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(new UserDetails(1, "test1@test.test", "First Name", "Middle Name",
                        "Last Name", null, true));

        Mockito.when(mockUserRepository.delete(1))
                .thenReturn(1);

        // Act
        int result = userService.delete(1);

        // Assert
        Assert.assertEquals(1, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void delete_Should_ThrowException_When_IdNotExists() {
        // Arrange
        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(null);
        // Act
        userService.delete(1);

        // Assert
        Mockito.verify(mockUserRepository, Mockito.never()).delete(1);
    }

    @Test
    public void getRatedBeers_Should_ReturnRatedBeerList_When_RatedBeersAndUserIdExist() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        RatedBeer ratedBeer = new RatedBeer(1, 4, beer, 1);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        Mockito.when(mockRatedBeerRepository.topRatedBeersByUserId(1))
                .thenReturn(Arrays.asList(
                        ratedBeer
                ));

        // Act
        List<RatedBeer> result = userService.getRatedBeers(1);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getRatedBeers_Should_ReturnEmptyList_When_RatedBeersNotExistAndUserIdExists() {
        // Arrange
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        // Act
        List<RatedBeer> result = userService.getRatedBeers(1);

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getRatedBeers_Should_ThrowException_When_UserIdNotExist() {
        // Arrange

        // Act
        userService.getRatedBeers(1);

        // Assert
        Mockito.verify(mockRatedBeerRepository, Mockito.never()).topRatedBeersByUserId(1);
    }

    @Test
    public void getMarkedBeers_Should_ReturnMarkedBeerList_When_MarkedBeersAndUserIdExist() {
        // Arrange
        Brewery brewery = new Brewery(1, "Brewery One");
        Country country = new Country(1, "Country One");
        Style style = new Style(1, "Pale Ale");
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);
        Beer beer = new Beer(1, "Beer One", brewery, country, 8.5, "Test Beer", style, 7.5, user, true, user, null);
        MarkedBeer markedBeer = new MarkedBeer(1, beer, 1, 1);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        Mockito.when(mockMarkedBeerRepository.markedBeersByUserId(1))
                .thenReturn(Arrays.asList(
                        markedBeer
                ));

        // Act
        List<MarkedBeer> result = userService.getMarkedBeers(1);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getMarkedBeers_Should_ReturnEmptyList_When_MarkedBeersNotExistAndUserIdExists() {
        // Arrange
        UserDetails user = new UserDetails(1, "test@test.test", "First Name", "Middle Name",
                "Last Name", null, true);

        Mockito.when(mockUserRepository.getById(1))
                .thenReturn(user);

        // Act
        List<MarkedBeer> result = userService.getMarkedBeers(1);

        // Assert
        Assert.assertEquals(0, result.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getMarkedBeers_Should_ThrowException_When_UserIdNotExist() {
        // Arrange

        // Act
        userService.getMarkedBeers(1);

        // Assert
        Mockito.verify(mockMarkedBeerRepository, Mockito.never()).markedBeersByUserId(1);
    }
}
