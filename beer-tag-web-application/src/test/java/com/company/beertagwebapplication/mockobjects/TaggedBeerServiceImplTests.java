package com.company.beertagwebapplication.mockobjects;

import com.company.beertagwebapplication.models.*;
import com.company.beertagwebapplication.repostories.TaggedBeerRepository;
import com.company.beertagwebapplication.services.TaggedBeersServiceImpl;
import com.company.beertagwebapplication.services.interfaces.TaggedBeersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TaggedBeerServiceImplTests {
    @Mock
    private TaggedBeerRepository mockTaggedBeerRepository;

    @InjectMocks
    private TaggedBeersServiceImpl taggedBeersService;

    @Test
    public void add_Should_CallRepositoryAdd_When_BeerIdNotTagged() {
        // Arrange
        TaggedBeer taggedBeer = new TaggedBeer(1,1, 1, 1);

        Mockito.when(mockTaggedBeerRepository.get(taggedBeer))
                .thenReturn(new ArrayList<>());

        // Act
        taggedBeersService.add(taggedBeer);

        // Assert
        Mockito.verify(mockTaggedBeerRepository, Mockito.times(1)).add(taggedBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void add_Should_ThrowException_When_TaggedBeerExists() {
        // Arrange
        TaggedBeer taggedBeer = new TaggedBeer(1,1, 1, 1);

        Mockito.when(mockTaggedBeerRepository.get(taggedBeer))
                .thenReturn(Arrays.asList(taggedBeer));

        // Act
        taggedBeersService.add(taggedBeer);

        // Assert
        Mockito.verify(mockTaggedBeerRepository, Mockito.never()).add(taggedBeer);
    }

    @Test
    public void remove_Should_CallRepositoryRemove_When_UntagBeer() {
        // Arrange
        TaggedBeer taggedBeer = new TaggedBeer(1,1, 1, 1);

        Mockito.when(mockTaggedBeerRepository.get(taggedBeer))
                .thenReturn(Arrays.asList(taggedBeer));

        Mockito.when(mockTaggedBeerRepository.remove(taggedBeer))
                .thenReturn(1);

        // Act
        int result = taggedBeersService.remove(taggedBeer);

        // Assert
        Assert.assertEquals(1, result);
        Mockito.verify(mockTaggedBeerRepository, Mockito.times(1)).remove(taggedBeer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void remove_ThrowException_When_TaggedBeerNotExists() {
        // Arrange
        TaggedBeer taggedBeer = new TaggedBeer(1,1, 1, 1);

        // Act
       taggedBeersService.remove(taggedBeer);

        // Assert
        Mockito.verify(mockTaggedBeerRepository, Mockito.never()).remove(taggedBeer);
    }
}
