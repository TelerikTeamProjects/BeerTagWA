-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time:  8 юли 2019 в 12:42
-- Версия на сървъра: 10.2.25-MariaDB-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myqaspac_beertag_rest_api_tests`
--

-- --------------------------------------------------------

--
-- Структура на таблица `authorities`
--

CREATE TABLE `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `beers`
--

CREATE TABLE `beers` (
  `beer_id` int(11) NOT NULL,
  `beer_name` varchar(50) NOT NULL,
  `brewery_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `ABV` double NOT NULL,
  `description` text DEFAULT NULL,
  `style_id` int(11) NOT NULL,
  `avg_rating` double NOT NULL DEFAULT 0,
  `owner` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT current_timestamp(),
  `date_modified` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `beer_images`
--

CREATE TABLE `beer_images` (
  `image_id` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `image` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `beer_styles`
--

CREATE TABLE `beer_styles` (
  `style_id` int(11) NOT NULL,
  `style_name` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `beer_tags`
--

CREATE TABLE `beer_tags` (
  `beer_tag_id` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `breweries`
--

CREATE TABLE `breweries` (
  `brewery_id` int(11) NOT NULL,
  `brewery_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `marked_beers`
--

CREATE TABLE `marked_beers` (
  `marked_beer_id` int(11) NOT NULL,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mark_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `marks`
--

CREATE TABLE `marks` (
  `mark_id` int(11) NOT NULL,
  `mark_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `rated_beers`
--

CREATE TABLE `rated_beers` (
  `beer_rating_id` int(11) NOT NULL,
  `beer_rating` tinyint(4) NOT NULL,
  `beer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Тригери `rated_beers`
--
DELIMITER $$
CREATE TRIGGER `beers_after_insert` AFTER INSERT ON `rated_beers` FOR EACH ROW BEGIN
    UPDATE beers
    SET avg_rating = (SELECT AVG(beer_rating) FROM rated_beers WHERE beer_id = NEW.beer_id GROUP BY beer_id), modified_by = NEW.user_id
    WHERE beer_id = NEW.beer_id;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура на таблица `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL,
  `tag_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `enabled` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `users_details`
--

CREATE TABLE `users_details` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(25) NOT NULL DEFAULT 'New register user',
  `middle_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) NOT NULL DEFAULT 'Registration only',
  `enabled` tinyint(4) DEFAULT 1,
  `image_id` int(11) DEFAULT NULL,
  `user_registered` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `user_images`
--

CREATE TABLE `user_images` (
  `image_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_name` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `image` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `authorities`
--
ALTER TABLE `authorities`
  ADD UNIQUE KEY `username_authority` (`username`,`authority`);

--
-- Indexes for table `beers`
--
ALTER TABLE `beers`
  ADD PRIMARY KEY (`beer_id`),
  ADD KEY `beers_users_user_id_fk` (`owner`),
  ADD KEY `beers_breweries_brewery_id_fk` (`brewery_id`),
  ADD KEY `beers_countries_countru_id_fk` (`country_id`),
  ADD KEY `beers_beer_styles_style_id_fk` (`style_id`),
  ADD KEY `beers_users_user_id_fk_2` (`modified_by`),
  ADD KEY `beers_beer_images_image_id_fk` (`image_id`);

--
-- Indexes for table `beer_images`
--
ALTER TABLE `beer_images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `images_beers_beer_id_fk` (`beer_id`);

--
-- Indexes for table `beer_styles`
--
ALTER TABLE `beer_styles`
  ADD PRIMARY KEY (`style_id`),
  ADD UNIQUE KEY `beer_styles_style_name_uindex` (`style_name`);

--
-- Indexes for table `beer_tags`
--
ALTER TABLE `beer_tags`
  ADD PRIMARY KEY (`beer_tag_id`),
  ADD UNIQUE KEY `beer_tags_beer_id_tag_id_uindex` (`beer_id`,`tag_id`),
  ADD KEY `beer_tags_beers_beer_id_fk` (`beer_id`),
  ADD KEY `beer_tags_users_user_id_fk` (`user_id`),
  ADD KEY `beer_tags_tags_tag_id_fk` (`tag_id`);

--
-- Indexes for table `breweries`
--
ALTER TABLE `breweries`
  ADD PRIMARY KEY (`brewery_id`),
  ADD UNIQUE KEY `breweries_brewery_uindex` (`brewery_name`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `marked_beers`
--
ALTER TABLE `marked_beers`
  ADD PRIMARY KEY (`marked_beer_id`),
  ADD KEY `marked_beers_users_user_id_fk` (`user_id`),
  ADD KEY `marked_beers_beers_beer_id_fk` (`beer_id`),
  ADD KEY `marked_beers_marks_mark_id_fk` (`mark_id`);

--
-- Indexes for table `marks`
--
ALTER TABLE `marks`
  ADD PRIMARY KEY (`mark_id`);

--
-- Indexes for table `rated_beers`
--
ALTER TABLE `rated_beers`
  ADD PRIMARY KEY (`beer_rating_id`),
  ADD UNIQUE KEY `Index 4` (`beer_id`,`user_id`),
  ADD KEY `rated_beers_beers_beer_id_fk` (`beer_id`),
  ADD KEY `rated_beers_users_user_id_fk` (`user_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `users_details`
--
ALTER TABLE `users_details`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_username_uindex` (`username`),
  ADD KEY `users_details_user_images_image_id_fk` (`image_id`);

--
-- Indexes for table `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `user_images_users_details_user_id_fk` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `beers`
--
ALTER TABLE `beers`
  MODIFY `beer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `beer_images`
--
ALTER TABLE `beer_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `beer_styles`
--
ALTER TABLE `beer_styles`
  MODIFY `style_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `beer_tags`
--
ALTER TABLE `beer_tags`
  MODIFY `beer_tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `breweries`
--
ALTER TABLE `breweries`
  MODIFY `brewery_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marked_beers`
--
ALTER TABLE `marked_beers`
  MODIFY `marked_beer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marks`
--
ALTER TABLE `marks`
  MODIFY `mark_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rated_beers`
--
ALTER TABLE `rated_beers`
  MODIFY `beer_rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_details`
--
ALTER TABLE `users_details`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_images`
--
ALTER TABLE `user_images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `beers`
--
ALTER TABLE `beers`
  ADD CONSTRAINT `beers_beer_images_image_id_fk` FOREIGN KEY (`image_id`) REFERENCES `beer_images` (`image_id`),
  ADD CONSTRAINT `beers_beer_styles_style_id_fk` FOREIGN KEY (`style_id`) REFERENCES `beer_styles` (`style_id`),
  ADD CONSTRAINT `beers_breweries_brewery_id_fk` FOREIGN KEY (`brewery_id`) REFERENCES `breweries` (`brewery_id`),
  ADD CONSTRAINT `beers_countries_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`),
  ADD CONSTRAINT `beers_users_details_user_id_fk` FOREIGN KEY (`owner`) REFERENCES `users_details` (`user_id`),
  ADD CONSTRAINT `beers_users_details_user_id_fk_2` FOREIGN KEY (`modified_by`) REFERENCES `users_details` (`user_id`);

--
-- Ограничения за таблица `beer_images`
--
ALTER TABLE `beer_images`
  ADD CONSTRAINT `images_beers_beer_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`);

--
-- Ограничения за таблица `beer_tags`
--
ALTER TABLE `beer_tags`
  ADD CONSTRAINT `beer_tags_beers_beer_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `beer_tags_tags_tag_id_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`),
  ADD CONSTRAINT `beer_tags_users_details_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_details` (`user_id`);

--
-- Ограничения за таблица `marked_beers`
--
ALTER TABLE `marked_beers`
  ADD CONSTRAINT `marked_beers_beers_beer_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  ADD CONSTRAINT `marked_beers_marks_mark_id_fk` FOREIGN KEY (`mark_id`) REFERENCES `marks` (`mark_id`),
  ADD CONSTRAINT `marked_beers_users_details_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_details` (`user_id`);

--
-- Ограничения за таблица `rated_beers`
--
ALTER TABLE `rated_beers`
  ADD CONSTRAINT `rated_beers_beers_beer_id_fk` FOREIGN KEY (`beer_id`) REFERENCES `beers` (`beer_id`),
  ADD CONSTRAINT `rated_beers_users_details_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_details` (`user_id`);

--
-- Ограничения за таблица `users_details`
--
ALTER TABLE `users_details`
  ADD CONSTRAINT `users_details_user_images_image_id_fk` FOREIGN KEY (`image_id`) REFERENCES `user_images` (`image_id`),
  ADD CONSTRAINT `users_details_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Ограничения за таблица `user_images`
--
ALTER TABLE `user_images`
  ADD CONSTRAINT `user_images_users_details_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users_details` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
