# Java Team Project

## [Project Description]()
Develop BEER TAG web application. BEER TAG enables users to manage all the beers that they have drank and want to drink. 
Each beer has detailed information about it from the ABV (alcohol by volume) to the style and description. Data is community driven and every beer lover can add new beers and edit missing information on already existing ones. Also, BEER TAG allows you to rate a beer and calculates average rating from different users.

## [Project Requirements]()

### [Web application]() 
**Public Part** 
The public part of the project should be visible without authentication. This includes the **application start page**, the **user login** and **user registration forms**, as well as **list of all beers** that have been added from different users. People that are not authenticated can not see any user specific details, neither they can interact with the website, they can only browse the beers and see list and details of them.

**Private Part (Users only)**
 - Registered users should have private part in the web application accessible after successful login.
The web application provides them with UI to **add/edit/delete** beers.
 - Each beer has **name**, **brewery** that produces it, **origin country**, **ABV**, **description**, **style (pre-defined)** and **picture**. 
Users can add tags to each beer. They can select among already added or create new. Each beer can be marked as **“drank”** and **“want to drink”**. 
When beer is marked as drank message should appear with some text, for example “Cheers!”.
 - Each user can rate a beer and average rating is calculated. Both ratings should be visible for the users.
 - All beers are listed and can be sorted by rating, ABV, alphabetically. The list can be filtered by tags, style, origin country.
 - There should be master-details view that will show beer details when one is selected.
 - It is our choice of design where and what properties to visualize, but all data should be visible somewhere.
 - The app should have **profile page** that shows user’s **photo** and **name** from their **contact** on the **device** and their top 3 most ranked beers, they’ve tasted.

**Administration Part**
 - **System administrators** should have administrative access to the system and permissions to administer all major information objects in the system, e.g. to **create/edit/delete users and other administrators**, to **edit/delete beers** and related data if they decide to.
 
 Follow us on our Trello Kanban Board https://<i></i>trello.com/b/t1aObz4F/beer-tagggs :eyes: :wink: :four_leaf_clover: